/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "version_software", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VersionSoftware.findAll", query = "SELECT v FROM VersionSoftware v"),
    @NamedQuery(name = "VersionSoftware.findByVersion", query = "SELECT v FROM VersionSoftware v WHERE v.versionSoftwarePK.version = :version"),
    @NamedQuery(name = "VersionSoftware.findByIdSoftware", query = "SELECT v FROM VersionSoftware v WHERE v.versionSoftwarePK.idSoftware = :idSoftware"),
    @NamedQuery(name = "VersionSoftware.findByFechaAlta", query = "SELECT v FROM VersionSoftware v WHERE v.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "VersionSoftware.findByObservacion", query = "SELECT v FROM VersionSoftware v WHERE v.observacion = :observacion"),
    @NamedQuery(name = "VersionSoftware.findByNombreArchivo", query = "SELECT v FROM VersionSoftware v WHERE v.nombreArchivo = :nombreArchivo"),
    @NamedQuery(name = "VersionSoftware.findByHashArchivo", query = "SELECT v FROM VersionSoftware v WHERE v.hashArchivo = :hashArchivo")})
public class VersionSoftware implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VersionSoftwarePK versionSoftwarePK;
    @Basic(optional = false)
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Basic(optional = false)
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "nombre_archivo")
    private String nombreArchivo;
    @Column(name = "hash_archivo")
    private String hashArchivo;
    @OneToMany(mappedBy = "versionSoftware")
    private Collection<Actualizacion> actualizacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "versionSoftware")
    private Collection<Instalacion> instalacionCollection;

    public VersionSoftware() {
    }

    public VersionSoftware(VersionSoftwarePK versionSoftwarePK) {
        this.versionSoftwarePK = versionSoftwarePK;
    }

    public VersionSoftware(VersionSoftwarePK versionSoftwarePK, Date fechaAlta, String observacion) {
        this.versionSoftwarePK = versionSoftwarePK;
        this.fechaAlta = fechaAlta;
        this.observacion = observacion;
    }

    public VersionSoftware(String version, int idSoftware) {
        this.versionSoftwarePK = new VersionSoftwarePK(version, idSoftware);
    }

    public VersionSoftwarePK getVersionSoftwarePK() {
        return versionSoftwarePK;
    }

    public void setVersionSoftwarePK(VersionSoftwarePK versionSoftwarePK) {
        this.versionSoftwarePK = versionSoftwarePK;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getHashArchivo() {
        return hashArchivo;
    }

    public void setHashArchivo(String hashArchivo) {
        this.hashArchivo = hashArchivo;
    }

    @XmlTransient
    public Collection<Actualizacion> getActualizacionCollection() {
        return actualizacionCollection;
    }

    public void setActualizacionCollection(Collection<Actualizacion> actualizacionCollection) {
        this.actualizacionCollection = actualizacionCollection;
    }

    @XmlTransient
    public Collection<Instalacion> getInstalacionCollection() {
        return instalacionCollection;
    }

    public void setInstalacionCollection(Collection<Instalacion> instalacionCollection) {
        this.instalacionCollection = instalacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (versionSoftwarePK != null ? versionSoftwarePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VersionSoftware)) {
            return false;
        }
        VersionSoftware other = (VersionSoftware) object;
        if ((this.versionSoftwarePK == null && other.versionSoftwarePK != null) || (this.versionSoftwarePK != null && !this.versionSoftwarePK.equals(other.versionSoftwarePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.trans.VersionSoftware[ versionSoftwarePK=" + versionSoftwarePK + " ]";
    }
    
}
