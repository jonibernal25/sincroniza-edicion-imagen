/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.utils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.io.IOUtils;
import py.com.sepsa.edicionimagen.generator.entities.info.ProductoImagen;
import py.com.sepsa.edicionimagen.generator.entities.info.ProductoImagenPK;

/**
 * Clase para el maenjo de las utilidades de repositorio de imagenes
 *
 * @author Jonathan D. Bernal Fernández
 */
public class AwsUtils {

    /**
     * SUFFIX
     */
    private static final String SUFFIX = "/";

    /**
     * Región del cliente
     */
    private static final String CLIENT_REGION = "us-east-2";

    /**
     * Nombre del bucket
     */
    private static final String BUCKET_NAME = "images.sepsa.com.py";

    public static ProductoImagen upload(String fileName, InputStream fileStream,
            long fileSize, String contentType, Integer prodId,
            ProductoImagen productoImage, Character type) throws Exception {

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                "AKIAYUPREVGRWXMXAMOG",
                "5OgkhhERb5Y4Lp20pNE8IorlMgr5xybB8ah3yzIj");

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(CLIENT_REGION)
                .build();

        ObjectMetadata s3ObjectMetadata = new ObjectMetadata();
        s3ObjectMetadata.setContentLength(fileSize);
        s3ObjectMetadata.setContentType(contentType);

        byte[] bytes = IOUtils.toByteArray(fileStream);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(bytes);

        InputStream firstClone = new ByteArrayInputStream(baos.toByteArray()); 
        InputStream secondClone = new ByteArrayInputStream(baos.toByteArray()); 

        s3Client.putObject(new PutObjectRequest(BUCKET_NAME, fileName, firstClone, s3ObjectMetadata)
                .withCannedAcl(CannedAccessControlList.PublicRead));

        
        int width = 2400;
        int height = 2400;
        
        try {
            BufferedImage bimg = ImageIO.read(secondClone);
            width = bimg.getWidth();
            height = bimg.getHeight();
        } catch (Exception e) {
        }

        if (prodId != null) {
            if(productoImage == null) {
                ProductoImagenPK pkPI = new ProductoImagenPK();
                pkPI.setIdProducto(prodId);
                productoImage = new ProductoImagen(pkPI);
                productoImage.setImgSrc(SUFFIX + fileName);
                productoImage.setHashImagen("NN");
                productoImage.setTipo(type);
                productoImage.setAlto(height);
                productoImage.setAncho(width);
                productoImage.setTipoAlmacenamiento(1);
                productoImage.setPrincipal('S');
            } else {
                productoImage.setImgSrc(SUFFIX + fileName);
                productoImage.setAlto(height);
                productoImage.setAncho(width);
                productoImage.setTipo(type);
            }
        }

        firstClone.close();
        secondClone.close();
        return productoImage;
    }
    
    public static boolean upload(String fileName, InputStream fileStream,
            long fileSize, String contentType) {

        boolean state = false;
        
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                "AKIAYUPREVGRWXMXAMOG",
                "5OgkhhERb5Y4Lp20pNE8IorlMgr5xybB8ah3yzIj");

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(CLIENT_REGION)
                .build();

        try {

            ObjectMetadata s3ObjectMetadata = new ObjectMetadata();
            s3ObjectMetadata.setContentLength(fileSize);
            s3ObjectMetadata.setContentType(contentType);
            
            byte[] bytes = IOUtils.toByteArray(fileStream);
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(bytes);
            
            InputStream firstClone = new ByteArrayInputStream(baos.toByteArray()); 
            
            s3Client.putObject(new PutObjectRequest(BUCKET_NAME, fileName, firstClone, s3ObjectMetadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
            
            state = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return state;
    }

    public static void createFolder(String folderName, String folderParent) {
        System.out.println("PADRE:"+folderParent+", HIJO:"+folderName);
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                "AKIAYUPREVGRWXMXAMOG",
                "5OgkhhERb5Y4Lp20pNE8IorlMgr5xybB8ah3yzIj");

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(CLIENT_REGION)
                .build();

        // create meta-data for your folder and set content-length to 0
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);

        // create empty content
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

        PutObjectRequest putObjectRequest;
        if (folderParent.equals("")) {
            // create a PutObjectRequest passing the folder name suffixed by /
            //putObjectRequest = new PutObjectRequest(bucketName,folderName + SUFFIX, emptyContent, metadata);
        } else {
            // create a PutObjectRequest passing the folder name suffixed by /
            putObjectRequest = new PutObjectRequest(BUCKET_NAME,
                    folderParent + SUFFIX + folderName + SUFFIX, emptyContent, metadata);
            // send request to S3 to create folder
            s3Client.putObject(putObjectRequest);
        }
    }

    public static boolean exitsDir(String mys3object) {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                "AKIAYUPREVGRWXMXAMOG",
                "5OgkhhERb5Y4Lp20pNE8IorlMgr5xybB8ah3yzIj");

        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(CLIENT_REGION)
                .build();

        boolean exists = false;
        try {

            ListObjectsRequest listRequest = new ListObjectsRequest()
                    .withBucketName(BUCKET_NAME).withPrefix(mys3object);

            ObjectListing objects = s3.listObjects(listRequest);
            
            List<S3ObjectSummary> summaries = objects.getObjectSummaries();
            exists = summaries != null && !summaries.isEmpty();

            if (exists) {
                System.out.println("Object \"" + BUCKET_NAME + "/" + mys3object + "\" exists!");
            } else {
                System.out.println("Object \"" + BUCKET_NAME + "/" + mys3object + "\" does not exist!");
            }
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
        return exists;
    }
    
    public static final String hashSha256(final InputStream stream) {
        String result = "N/A";
        
        try {
            final byte[] buffer = new byte[1024 * 1024];
            final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            int bytesRead = 0;
            while ((bytesRead = stream.read(buffer)) >= 0) {
                if (bytesRead > 0) {
                    sha256.update(buffer, 0, bytesRead);
                }
            }
            byte[] digest = sha256.digest();
            
            result = String.format("%064x", new BigInteger(1, digest));
        } catch (Exception e) {
        }
        
        return result;
    }
}
