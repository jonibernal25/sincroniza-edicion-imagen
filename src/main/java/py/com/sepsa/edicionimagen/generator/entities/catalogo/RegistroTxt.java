/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "registro_txt", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegistroTxt.findAll", query = "SELECT r FROM RegistroTxt r"),
    @NamedQuery(name = "RegistroTxt.findByNroLinea", query = "SELECT r FROM RegistroTxt r WHERE r.nroLinea = :nroLinea"),
    @NamedQuery(name = "RegistroTxt.findByCantidadCampo", query = "SELECT r FROM RegistroTxt r WHERE r.cantidadCampo = :cantidadCampo")})
public class RegistroTxt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "nro_linea")
    private Integer nroLinea;
    @Basic(optional = false)
    @Column(name = "cantidad_campo")
    private int cantidadCampo;
    @OneToMany(mappedBy = "nroLinea")
    private Collection<Caracteristica> caracteristicaCollection;

    public RegistroTxt() {
    }

    public RegistroTxt(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public RegistroTxt(Integer nroLinea, int cantidadCampo) {
        this.nroLinea = nroLinea;
        this.cantidadCampo = cantidadCampo;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public int getCantidadCampo() {
        return cantidadCampo;
    }

    public void setCantidadCampo(int cantidadCampo) {
        this.cantidadCampo = cantidadCampo;
    }

    @XmlTransient
    public Collection<Caracteristica> getCaracteristicaCollection() {
        return caracteristicaCollection;
    }

    public void setCaracteristicaCollection(Collection<Caracteristica> caracteristicaCollection) {
        this.caracteristicaCollection = caracteristicaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nroLinea != null ? nroLinea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroTxt)) {
            return false;
        }
        RegistroTxt other = (RegistroTxt) object;
        if ((this.nroLinea == null && other.nroLinea != null) || (this.nroLinea != null && !this.nroLinea.equals(other.nroLinea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.RegistroTxt[ nroLinea=" + nroLinea + " ]";
    }
    
}
