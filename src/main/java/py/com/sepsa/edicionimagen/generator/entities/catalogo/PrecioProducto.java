/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "precio_producto", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrecioProducto.findAll", query = "SELECT p FROM PrecioProducto p"),
    @NamedQuery(name = "PrecioProducto.findByIdProducto", query = "SELECT p FROM PrecioProducto p WHERE p.idProducto = :idProducto"),
    @NamedQuery(name = "PrecioProducto.findByIdComprador", query = "SELECT p FROM PrecioProducto p WHERE p.idComprador = :idComprador"),
    @NamedQuery(name = "PrecioProducto.findByPrecio", query = "SELECT p FROM PrecioProducto p WHERE p.precio = :precio"),
    @NamedQuery(name = "PrecioProducto.findByIdMoneda", query = "SELECT p FROM PrecioProducto p WHERE p.idMoneda = :idMoneda"),
    @NamedQuery(name = "PrecioProducto.findById", query = "SELECT p FROM PrecioProducto p WHERE p.id = :id"),
    @NamedQuery(name = "PrecioProducto.findByFechaInsercion", query = "SELECT p FROM PrecioProducto p WHERE p.fechaInsercion = :fechaInsercion"),
    @NamedQuery(name = "PrecioProducto.findByFechaModificacion", query = "SELECT p FROM PrecioProducto p WHERE p.fechaModificacion = :fechaModificacion")})
public class PrecioProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;
    @Column(name = "id_comprador")
    private Integer idComprador;
    @Basic(optional = false)
    @Column(name = "precio")
    private BigInteger precio;
    @Basic(optional = false)
    @Column(name = "id_moneda")
    private int idMoneda;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @JoinColumn(name = "id_canal_venta", referencedColumnName = "id")
    @ManyToOne
    private CanalVenta idCanalVenta;

    public PrecioProducto() {
    }

    public PrecioProducto(Integer id) {
        this.id = id;
    }

    public PrecioProducto(Integer id, int idProducto, BigInteger precio, int idMoneda) {
        this.id = id;
        this.idProducto = idProducto;
        this.precio = precio;
        this.idMoneda = idMoneda;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public BigInteger getPrecio() {
        return precio;
    }

    public void setPrecio(BigInteger precio) {
        this.precio = precio;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public CanalVenta getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(CanalVenta idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrecioProducto)) {
            return false;
        }
        PrecioProducto other = (PrecioProducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.PrecioProducto[ id=" + id + " ]";
    }
    
}
