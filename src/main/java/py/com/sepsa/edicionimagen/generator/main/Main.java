/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.main;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.EdicionImagen;
import py.com.sepsa.edicionimagen.generator.utils.BdUtils;
import py.com.sepsa.edicionimagen.generator.utils.JpaUtil;
import py.com.sepsa.edicionimagen.generator.utils.LogHandler;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class Main {

    /**
     * Ruta al archivo de configuración del sistema
     */
    private static String config = "config.json";
    //private static String config = "C:\\Users\\Sepsa\\Desktop\\EdicionImagen\\config.json";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            File fileConfig = new File(config);

            if (!fileConfig.exists()) {
                System.out.println("No existe el archivo de configuración");
                System.exit(1);
            }

            String configData = IOUtils.toString(new FileInputStream(fileConfig));

            JSONObject json = new JSONObject(configData);
            JpaUtil.setURL_SEPSA(json.getString("urlSepsa"));
            JpaUtil.setDB_USER_SEPSA(json.getString("userSepsa"));
            JpaUtil.setDB_PASS_SEPSA(json.getString("passwordSepsa"));
            Long sleepTime = json.optLong("timeSleep", 90000);
            String user = json.getString("user");
            
            BdUtils bdUtils = new BdUtils();
            Download download = new Download(bdUtils);
            
            List<EdicionImagen> list;
            
            while (true) {
                try {
                    
                    list = bdUtils.findPendienteSincronizacion(0, 5);
                    
                    while (list != null && !list.isEmpty()) {
                        
                        for (EdicionImagen edicionImagen : list) {
                            download.process(edicionImagen, user);
                        }

                        list = bdUtils.findPendienteSincronizacion(0, 5);
                    }
                    
                } catch(Throwable ex) {
                    LogHandler.logFatal("Se ha producido el siguietn error:", ex);
                }
                
                try {
                    Thread.sleep(sleepTime);
                } catch(Exception ex) {
                    LogHandler.logFatal("Se ha producido el siguietn error:", ex);
                }
            }
        } catch(Exception ex) {
            LogHandler.logFatal("Se ha producido un error:", ex);
        }
    }
    
}
