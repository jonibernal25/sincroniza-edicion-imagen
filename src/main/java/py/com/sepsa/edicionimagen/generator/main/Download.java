/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.main;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.io.IOUtils;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.EdicionImagen;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.HistoricoEdicionImagen;
import py.com.sepsa.edicionimagen.generator.utils.BdUtils;
import py.com.sepsa.edicionimagen.generator.utils.LogHandler;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.Etiqueta;
import py.com.sepsa.edicionimagen.generator.entities.info.Producto;
import py.com.sepsa.edicionimagen.generator.entities.info.ProductoImagen;
import py.com.sepsa.edicionimagen.generator.entities.trans.Usuario;
import py.com.sepsa.edicionimagen.generator.utils.AwsUtils;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class Download {

    public Download(BdUtils bdUtils) {
        this.bdUtils = bdUtils;
    }
    
    /**
     * Manejador de base de datos
     */
    private BdUtils bdUtils;
    
    /**
     * Procesa la descarga de imagen
     * @param edicionImagen Edicion Imagen
     * @param user Usuario asociado a la operación
     * @throws Throwable
     */
    public void process(EdicionImagen edicionImagen, String user) throws Throwable {
        
        List<HistoricoEdicionImagen> list = bdUtils
                .findHistoricoEdicionImagen(edicionImagen.getId(), "CARGADO");
        
        HistoricoEdicionImagen cargado = list == null
                || list.isEmpty()
                ? null
                : list.get(0);
        
        if(cargado == null) {
            LogHandler.logError("No se encuentra la etiqueta CARGADO para el producto");
            return;
        }
        
        Usuario usuario = bdUtils.findUsuario(user);
        
        if(usuario == null) {
            LogHandler.logError(String.format("No se encuentra el usuario: %s", user));
            return;
        }
        
        Producto tmp = bdUtils.findProducto(edicionImagen.getIdProducto());
        Producto producto = bdUtils.findProducto(tmp.getCodigoGtin());
        if(producto == null) {
            producto = tmp;
        }
        
        String ruc = producto.getIdPersona().getRuc();
        StringTokenizer st = new StringTokenizer(cargado.getNombreArchivo(), ".");
        String fileName = String.format("%s.jpg", st.nextToken().trim());
        String pathFile = String.format("%s/%s/%s", ruc,
                producto.getCodigoGtin(), fileName);
        
        Etiqueta etiqueta = bdUtils.findEtiqueta("COPIADO CATALOGO");
        
        if(etiqueta == null) {
            LogHandler.logError("No se encuentra la etiqueta: COPIADO CATALOGO");
            return;
        }
        
        HistoricoEdicionImagen hei = edicionImagen
                .getIdHistoricoEdicionImagen();
        
        HttpURLConnection httpConn = GET(hei.getUrl().replaceAll(" ", "%20"));

        InputStream is = httpConn.getInputStream();
        byte[] bytes = IOUtils.toByteArray(is);
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ProductoImagen productoImagen = AwsUtils.upload(pathFile,
                bais, bytes.length, httpConn.getContentType(),
                producto.getId(), null, 'O');
                
        int id = bdUtils.nextValueProductoImagen();
        productoImagen.getProductoImagenPK().setId(id);
        bdUtils.create(productoImagen);
        
        HistoricoEdicionImagen descarga = 
                new HistoricoEdicionImagen();
        descarga.setDispositivo(hei.getDispositivo());
        descarga.setFecha(Calendar.getInstance().getTime());
        descarga.setHash(hei.getHash());
        descarga.setIdEdicionImagen(edicionImagen);
        descarga.setIdEtiqueta(etiqueta);
        descarga.setIdUsuario(usuario);
        descarga.setIdUsuarioEditor(hei.getIdUsuarioEditor());
        descarga.setNombreArchivo(fileName);
        descarga.setUrl(hei.getUrl());
        
        bdUtils.create(descarga);
        
        LogHandler.logDebug(String.format("Imagen Copiada, IdProducto:%s,"
                + "Nombre Archivo:%s",
                producto.getId(), pathFile));
        is.close();
        bais.close();
    }
    
    /**
     * GET para servicios
     *
     * @param url URL
     * @return Conexión HTTP
     */
    private HttpURLConnection GET(String url) {
        
        HttpURLConnection httpConn = null;
        
        try {
            
            URL restServiceURL = new URL(url);
            
            httpConn = (HttpURLConnection) restServiceURL.openConnection();
            
            httpConn.setUseCaches(false);
            
            httpConn.setRequestMethod(Method.GET.toString());
            
            httpConn.setConnectTimeout(30000);
            
        } catch (IOException ex) {
            LogHandler.logFatal("Se ha producido un error", ex);
        }
        
        return httpConn;
    }
    
    /**
     * Métodos para el request
     */
    public enum Method {
        GET, POST, PUT, PATCH, HEAD
    }
}
