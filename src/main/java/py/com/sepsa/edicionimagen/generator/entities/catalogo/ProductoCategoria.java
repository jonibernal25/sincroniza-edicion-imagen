/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_categoria", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoCategoria.findAll", query = "SELECT p FROM ProductoCategoria p"),
    @NamedQuery(name = "ProductoCategoria.findByIdProducto", query = "SELECT p FROM ProductoCategoria p WHERE p.productoCategoriaPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoCategoria.findByIdTipoCategoria", query = "SELECT p FROM ProductoCategoria p WHERE p.productoCategoriaPK.idTipoCategoria = :idTipoCategoria"),
    @NamedQuery(name = "ProductoCategoria.findByIdPersona", query = "SELECT p FROM ProductoCategoria p WHERE p.idPersona = :idPersona")})
public class ProductoCategoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoCategoriaPK productoCategoriaPK;
    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Categoria idCategoria;
    @JoinColumn(name = "id_tipo_categoria", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoCategoria tipoCategoria;

    public ProductoCategoria() {
    }

    public ProductoCategoria(ProductoCategoriaPK productoCategoriaPK) {
        this.productoCategoriaPK = productoCategoriaPK;
    }

    public ProductoCategoria(ProductoCategoriaPK productoCategoriaPK, int idPersona) {
        this.productoCategoriaPK = productoCategoriaPK;
        this.idPersona = idPersona;
    }

    public ProductoCategoria(int idProducto, int idTipoCategoria) {
        this.productoCategoriaPK = new ProductoCategoriaPK(idProducto, idTipoCategoria);
    }

    public ProductoCategoriaPK getProductoCategoriaPK() {
        return productoCategoriaPK;
    }

    public void setProductoCategoriaPK(ProductoCategoriaPK productoCategoriaPK) {
        this.productoCategoriaPK = productoCategoriaPK;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public Categoria getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Categoria idCategoria) {
        this.idCategoria = idCategoria;
    }

    public TipoCategoria getTipoCategoria() {
        return tipoCategoria;
    }

    public void setTipoCategoria(TipoCategoria tipoCategoria) {
        this.tipoCategoria = tipoCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoCategoriaPK != null ? productoCategoriaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoCategoria)) {
            return false;
        }
        ProductoCategoria other = (ProductoCategoria) object;
        if ((this.productoCategoriaPK == null && other.productoCategoriaPK != null) || (this.productoCategoriaPK != null && !this.productoCategoriaPK.equals(other.productoCategoriaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProductoCategoria[ productoCategoriaPK=" + productoCategoriaPK + " ]";
    }
    
}
