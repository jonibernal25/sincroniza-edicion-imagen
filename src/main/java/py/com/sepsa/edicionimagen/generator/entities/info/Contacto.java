/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.info;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "contacto", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contacto.findAll", query = "SELECT c FROM Contacto c"),
    @NamedQuery(name = "Contacto.findByIdContacto", query = "SELECT c FROM Contacto c WHERE c.idContacto = :idContacto"),
    @NamedQuery(name = "Contacto.findByEstado", query = "SELECT c FROM Contacto c WHERE c.estado = :estado")})
public class Contacto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_contacto")
    private Integer idContacto;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinTable(name = "local_contacto", joinColumns = {
        @JoinColumn(name = "id_contacto", referencedColumnName = "id_contacto")}, inverseJoinColumns = {
        @JoinColumn(name = "id_local", referencedColumnName = "id"),
        @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")})
    @ManyToMany
    private Collection<Local> localCollection;
    @JoinTable(name = "persona_contacto", joinColumns = {
        @JoinColumn(name = "id_contacto", referencedColumnName = "id_contacto")}, inverseJoinColumns = {
        @JoinColumn(name = "id_persona", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Persona> personaCollection;
    @JoinColumn(name = "id_cargo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cargo idCargo;
    @JoinColumn(name = "id_contacto", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Persona persona;
    @JoinColumn(name = "id_tipo_contacto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoContacto idTipoContacto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contacto")
    private Collection<ContactoTelefono> contactoTelefonoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contacto")
    private Collection<ContactoEmail> contactoEmailCollection;

    public Contacto() {
    }

    public Contacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Contacto(Integer idContacto, Character estado) {
        this.idContacto = idContacto;
        this.estado = estado;
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<Local> getLocalCollection() {
        return localCollection;
    }

    public void setLocalCollection(Collection<Local> localCollection) {
        this.localCollection = localCollection;
    }

    @XmlTransient
    public Collection<Persona> getPersonaCollection() {
        return personaCollection;
    }

    public void setPersonaCollection(Collection<Persona> personaCollection) {
        this.personaCollection = personaCollection;
    }

    public Cargo getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Cargo idCargo) {
        this.idCargo = idCargo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public TipoContacto getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(TipoContacto idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    @XmlTransient
    public Collection<ContactoTelefono> getContactoTelefonoCollection() {
        return contactoTelefonoCollection;
    }

    public void setContactoTelefonoCollection(Collection<ContactoTelefono> contactoTelefonoCollection) {
        this.contactoTelefonoCollection = contactoTelefonoCollection;
    }

    @XmlTransient
    public Collection<ContactoEmail> getContactoEmailCollection() {
        return contactoEmailCollection;
    }

    public void setContactoEmailCollection(Collection<ContactoEmail> contactoEmailCollection) {
        this.contactoEmailCollection = contactoEmailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContacto != null ? idContacto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contacto)) {
            return false;
        }
        Contacto other = (Contacto) object;
        if ((this.idContacto == null && other.idContacto != null) || (this.idContacto != null && !this.idContacto.equals(other.idContacto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.info.Contacto[ idContacto=" + idContacto + " ]";
    }
    
}
