/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "conversion_venta_oc", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConversionVentaOc.findAll", query = "SELECT c FROM ConversionVentaOc c"),
    @NamedQuery(name = "ConversionVentaOc.findByIdProducto", query = "SELECT c FROM ConversionVentaOc c WHERE c.idProducto = :idProducto"),
    @NamedQuery(name = "ConversionVentaOc.findByUnidadMedidaVenta", query = "SELECT c FROM ConversionVentaOc c WHERE c.unidadMedidaVenta = :unidadMedidaVenta"),
    @NamedQuery(name = "ConversionVentaOc.findByUnidadMedidaOc", query = "SELECT c FROM ConversionVentaOc c WHERE c.unidadMedidaOc = :unidadMedidaOc"),
    @NamedQuery(name = "ConversionVentaOc.findByConversionVentaOc", query = "SELECT c FROM ConversionVentaOc c WHERE c.conversionVentaOc = :conversionVentaOc"),
    @NamedQuery(name = "ConversionVentaOc.findByEtiquetaUnidadMedida", query = "SELECT c FROM ConversionVentaOc c WHERE c.etiquetaUnidadMedida = :etiquetaUnidadMedida"),
    @NamedQuery(name = "ConversionVentaOc.findByPresentacion", query = "SELECT c FROM ConversionVentaOc c WHERE c.presentacion = :presentacion")})
public class ConversionVentaOc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_producto")
    private Integer idProducto;
    @Basic(optional = false)
    @Column(name = "unidad_medida_venta")
    private String unidadMedidaVenta;
    @Basic(optional = false)
    @Column(name = "unidad_medida_oc")
    private String unidadMedidaOc;
    @Basic(optional = false)
    @Column(name = "conversion_venta_oc")
    private BigInteger conversionVentaOc;
    @Basic(optional = false)
    @Column(name = "etiqueta_unidad_medida")
    private String etiquetaUnidadMedida;
    @Basic(optional = false)
    @Column(name = "presentacion")
    private String presentacion;

    public ConversionVentaOc() {
    }

    public ConversionVentaOc(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public ConversionVentaOc(Integer idProducto, String unidadMedidaVenta, String unidadMedidaOc, BigInteger conversionVentaOc, String etiquetaUnidadMedida, String presentacion) {
        this.idProducto = idProducto;
        this.unidadMedidaVenta = unidadMedidaVenta;
        this.unidadMedidaOc = unidadMedidaOc;
        this.conversionVentaOc = conversionVentaOc;
        this.etiquetaUnidadMedida = etiquetaUnidadMedida;
        this.presentacion = presentacion;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getUnidadMedidaVenta() {
        return unidadMedidaVenta;
    }

    public void setUnidadMedidaVenta(String unidadMedidaVenta) {
        this.unidadMedidaVenta = unidadMedidaVenta;
    }

    public String getUnidadMedidaOc() {
        return unidadMedidaOc;
    }

    public void setUnidadMedidaOc(String unidadMedidaOc) {
        this.unidadMedidaOc = unidadMedidaOc;
    }

    public BigInteger getConversionVentaOc() {
        return conversionVentaOc;
    }

    public void setConversionVentaOc(BigInteger conversionVentaOc) {
        this.conversionVentaOc = conversionVentaOc;
    }

    public String getEtiquetaUnidadMedida() {
        return etiquetaUnidadMedida;
    }

    public void setEtiquetaUnidadMedida(String etiquetaUnidadMedida) {
        this.etiquetaUnidadMedida = etiquetaUnidadMedida;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConversionVentaOc)) {
            return false;
        }
        ConversionVentaOc other = (ConversionVentaOc) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ConversionVentaOc[ idProducto=" + idProducto + " ]";
    }
    
}
