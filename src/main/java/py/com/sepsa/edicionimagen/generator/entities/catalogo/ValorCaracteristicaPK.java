/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ValorCaracteristicaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @Column(name = "id_producto_caracteristica")
    private int idProductoCaracteristica;

    public ValorCaracteristicaPK() {
    }

    public ValorCaracteristicaPK(int id, int idProductoCaracteristica) {
        this.id = id;
        this.idProductoCaracteristica = idProductoCaracteristica;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProductoCaracteristica() {
        return idProductoCaracteristica;
    }

    public void setIdProductoCaracteristica(int idProductoCaracteristica) {
        this.idProductoCaracteristica = idProductoCaracteristica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) idProductoCaracteristica;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValorCaracteristicaPK)) {
            return false;
        }
        ValorCaracteristicaPK other = (ValorCaracteristicaPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.idProductoCaracteristica != other.idProductoCaracteristica) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ValorCaracteristicaPK[ id=" + id + ", idProductoCaracteristica=" + idProductoCaracteristica + " ]";
    }
    
}
