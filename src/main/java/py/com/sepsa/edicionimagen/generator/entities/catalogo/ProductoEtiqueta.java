/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_etiqueta", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoEtiqueta.findAll", query = "SELECT p FROM ProductoEtiqueta p"),
    @NamedQuery(name = "ProductoEtiqueta.findByIdProducto", query = "SELECT p FROM ProductoEtiqueta p WHERE p.productoEtiquetaPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoEtiqueta.findByIdEtiqueta", query = "SELECT p FROM ProductoEtiqueta p WHERE p.productoEtiquetaPK.idEtiqueta = :idEtiqueta"),
    @NamedQuery(name = "ProductoEtiqueta.findByInicioOperacion", query = "SELECT p FROM ProductoEtiqueta p WHERE p.inicioOperacion = :inicioOperacion"),
    @NamedQuery(name = "ProductoEtiqueta.findByFinOperacion", query = "SELECT p FROM ProductoEtiqueta p WHERE p.finOperacion = :finOperacion"),
    @NamedQuery(name = "ProductoEtiqueta.findByIdUsuario", query = "SELECT p FROM ProductoEtiqueta p WHERE p.idUsuario = :idUsuario")})
public class ProductoEtiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoEtiquetaPK productoEtiquetaPK;
    @Basic(optional = false)
    @Column(name = "inicio_operacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioOperacion;
    @Basic(optional = false)
    @Column(name = "fin_operacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finOperacion;
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @JoinColumn(name = "id_etiqueta", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Etiqueta etiqueta;
    @JoinColumn(name = "id_operacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private OperacionCatalogo idOperacion;

    public ProductoEtiqueta() {
    }

    public ProductoEtiqueta(ProductoEtiquetaPK productoEtiquetaPK) {
        this.productoEtiquetaPK = productoEtiquetaPK;
    }

    public ProductoEtiqueta(ProductoEtiquetaPK productoEtiquetaPK, Date inicioOperacion, Date finOperacion, int idUsuario) {
        this.productoEtiquetaPK = productoEtiquetaPK;
        this.inicioOperacion = inicioOperacion;
        this.finOperacion = finOperacion;
        this.idUsuario = idUsuario;
    }

    public ProductoEtiqueta(int idProducto, int idEtiqueta) {
        this.productoEtiquetaPK = new ProductoEtiquetaPK(idProducto, idEtiqueta);
    }

    public ProductoEtiquetaPK getProductoEtiquetaPK() {
        return productoEtiquetaPK;
    }

    public void setProductoEtiquetaPK(ProductoEtiquetaPK productoEtiquetaPK) {
        this.productoEtiquetaPK = productoEtiquetaPK;
    }

    public Date getInicioOperacion() {
        return inicioOperacion;
    }

    public void setInicioOperacion(Date inicioOperacion) {
        this.inicioOperacion = inicioOperacion;
    }

    public Date getFinOperacion() {
        return finOperacion;
    }

    public void setFinOperacion(Date finOperacion) {
        this.finOperacion = finOperacion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Etiqueta getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(Etiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    public OperacionCatalogo getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(OperacionCatalogo idOperacion) {
        this.idOperacion = idOperacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoEtiquetaPK != null ? productoEtiquetaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoEtiqueta)) {
            return false;
        }
        ProductoEtiqueta other = (ProductoEtiqueta) object;
        if ((this.productoEtiquetaPK == null && other.productoEtiquetaPK != null) || (this.productoEtiquetaPK != null && !this.productoEtiquetaPK.equals(other.productoEtiquetaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProductoEtiqueta[ productoEtiquetaPK=" + productoEtiquetaPK + " ]";
    }
    
}
