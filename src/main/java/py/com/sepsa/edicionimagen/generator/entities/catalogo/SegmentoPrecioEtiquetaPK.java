/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class SegmentoPrecioEtiquetaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_segmento_precio")
    private int idSegmentoPrecio;
    @Basic(optional = false)
    @Column(name = "id_etiqueta")
    private int idEtiqueta;

    public SegmentoPrecioEtiquetaPK() {
    }

    public SegmentoPrecioEtiquetaPK(int idSegmentoPrecio, int idEtiqueta) {
        this.idSegmentoPrecio = idSegmentoPrecio;
        this.idEtiqueta = idEtiqueta;
    }

    public int getIdSegmentoPrecio() {
        return idSegmentoPrecio;
    }

    public void setIdSegmentoPrecio(int idSegmentoPrecio) {
        this.idSegmentoPrecio = idSegmentoPrecio;
    }

    public int getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(int idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idSegmentoPrecio;
        hash += (int) idEtiqueta;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SegmentoPrecioEtiquetaPK)) {
            return false;
        }
        SegmentoPrecioEtiquetaPK other = (SegmentoPrecioEtiquetaPK) object;
        if (this.idSegmentoPrecio != other.idSegmentoPrecio) {
            return false;
        }
        if (this.idEtiqueta != other.idEtiqueta) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.SegmentoPrecioEtiquetaPK[ idSegmentoPrecio=" + idSegmentoPrecio + ", idEtiqueta=" + idEtiqueta + " ]";
    }
    
}
