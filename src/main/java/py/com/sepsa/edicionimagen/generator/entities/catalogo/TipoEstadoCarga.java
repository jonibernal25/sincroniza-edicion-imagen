/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tipo_estado_carga", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoEstadoCarga.findAll", query = "SELECT t FROM TipoEstadoCarga t"),
    @NamedQuery(name = "TipoEstadoCarga.findById", query = "SELECT t FROM TipoEstadoCarga t WHERE t.id = :id"),
    @NamedQuery(name = "TipoEstadoCarga.findByDescripcion", query = "SELECT t FROM TipoEstadoCarga t WHERE t.descripcion = :descripcion")})
public class TipoEstadoCarga implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idTipoEstadoCarga")
    private Collection<ProductoSupervisado> productoSupervisadoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoEstadoCarga")
    private Collection<EstadoCarga> estadoCargaCollection;

    public TipoEstadoCarga() {
    }

    public TipoEstadoCarga(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<ProductoSupervisado> getProductoSupervisadoCollection() {
        return productoSupervisadoCollection;
    }

    public void setProductoSupervisadoCollection(Collection<ProductoSupervisado> productoSupervisadoCollection) {
        this.productoSupervisadoCollection = productoSupervisadoCollection;
    }

    @XmlTransient
    public Collection<EstadoCarga> getEstadoCargaCollection() {
        return estadoCargaCollection;
    }

    public void setEstadoCargaCollection(Collection<EstadoCarga> estadoCargaCollection) {
        this.estadoCargaCollection = estadoCargaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEstadoCarga)) {
            return false;
        }
        TipoEstadoCarga other = (TipoEstadoCarga) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.TipoEstadoCarga[ id=" + id + " ]";
    }
    
}
