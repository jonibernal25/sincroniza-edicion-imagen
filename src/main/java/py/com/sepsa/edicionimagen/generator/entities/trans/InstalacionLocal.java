/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "instalacion_local", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstalacionLocal.findAll", query = "SELECT i FROM InstalacionLocal i"),
    @NamedQuery(name = "InstalacionLocal.findByIdInstalacion", query = "SELECT i FROM InstalacionLocal i WHERE i.instalacionLocalPK.idInstalacion = :idInstalacion"),
    @NamedQuery(name = "InstalacionLocal.findByIdPersona", query = "SELECT i FROM InstalacionLocal i WHERE i.instalacionLocalPK.idPersona = :idPersona"),
    @NamedQuery(name = "InstalacionLocal.findByIdLocal", query = "SELECT i FROM InstalacionLocal i WHERE i.instalacionLocalPK.idLocal = :idLocal"),
    @NamedQuery(name = "InstalacionLocal.findByEstado", query = "SELECT i FROM InstalacionLocal i WHERE i.estado = :estado")})
public class InstalacionLocal implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InstalacionLocalPK instalacionLocalPK;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_instalacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Instalacion instalacion;

    public InstalacionLocal() {
    }

    public InstalacionLocal(InstalacionLocalPK instalacionLocalPK) {
        this.instalacionLocalPK = instalacionLocalPK;
    }

    public InstalacionLocal(InstalacionLocalPK instalacionLocalPK, Character estado) {
        this.instalacionLocalPK = instalacionLocalPK;
        this.estado = estado;
    }

    public InstalacionLocal(int idInstalacion, int idPersona, int idLocal) {
        this.instalacionLocalPK = new InstalacionLocalPK(idInstalacion, idPersona, idLocal);
    }

    public InstalacionLocalPK getInstalacionLocalPK() {
        return instalacionLocalPK;
    }

    public void setInstalacionLocalPK(InstalacionLocalPK instalacionLocalPK) {
        this.instalacionLocalPK = instalacionLocalPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Instalacion getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(Instalacion instalacion) {
        this.instalacion = instalacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instalacionLocalPK != null ? instalacionLocalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstalacionLocal)) {
            return false;
        }
        InstalacionLocal other = (InstalacionLocal) object;
        if ((this.instalacionLocalPK == null && other.instalacionLocalPK != null) || (this.instalacionLocalPK != null && !this.instalacionLocalPK.equals(other.instalacionLocalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.trans.InstalacionLocal[ instalacionLocalPK=" + instalacionLocalPK + " ]";
    }
    
}
