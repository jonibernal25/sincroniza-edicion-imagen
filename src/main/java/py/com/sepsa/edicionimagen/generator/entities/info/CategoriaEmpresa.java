/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.info;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "categoria_empresa", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaEmpresa.findAll", query = "SELECT c FROM CategoriaEmpresa c"),
    @NamedQuery(name = "CategoriaEmpresa.findById", query = "SELECT c FROM CategoriaEmpresa c WHERE c.id = :id"),
    @NamedQuery(name = "CategoriaEmpresa.findByDescripcion", query = "SELECT c FROM CategoriaEmpresa c WHERE c.descripcion = :descripcion")})
public class CategoriaEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinTable(name = "categoria_empresa_persona", joinColumns = {
        @JoinColumn(name = "id_categoria_empresa", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_persona", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Persona> personaCollection;

    public CategoriaEmpresa() {
    }

    public CategoriaEmpresa(Integer id) {
        this.id = id;
    }

    public CategoriaEmpresa(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<Persona> getPersonaCollection() {
        return personaCollection;
    }

    public void setPersonaCollection(Collection<Persona> personaCollection) {
        this.personaCollection = personaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaEmpresa)) {
            return false;
        }
        CategoriaEmpresa other = (CategoriaEmpresa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.info.CategoriaEmpresa[ id=" + id + " ]";
    }
    
}
