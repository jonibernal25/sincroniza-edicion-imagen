/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class CaracteristicaPersonaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @Column(name = "id_caracteristica")
    private int idCaracteristica;
    @Basic(optional = false)
    @Column(name = "id_tipo_caracteristica")
    private int idTipoCaracteristica;
    @Basic(optional = false)
    @Column(name = "id_comprador")
    private int idComprador;

    public CaracteristicaPersonaPK() {
    }

    public CaracteristicaPersonaPK(int id, int idCaracteristica, int idTipoCaracteristica, int idComprador) {
        this.id = id;
        this.idCaracteristica = idCaracteristica;
        this.idTipoCaracteristica = idTipoCaracteristica;
        this.idComprador = idComprador;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCaracteristica() {
        return idCaracteristica;
    }

    public void setIdCaracteristica(int idCaracteristica) {
        this.idCaracteristica = idCaracteristica;
    }

    public int getIdTipoCaracteristica() {
        return idTipoCaracteristica;
    }

    public void setIdTipoCaracteristica(int idTipoCaracteristica) {
        this.idTipoCaracteristica = idTipoCaracteristica;
    }

    public int getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(int idComprador) {
        this.idComprador = idComprador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) idCaracteristica;
        hash += (int) idTipoCaracteristica;
        hash += (int) idComprador;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaracteristicaPersonaPK)) {
            return false;
        }
        CaracteristicaPersonaPK other = (CaracteristicaPersonaPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.idCaracteristica != other.idCaracteristica) {
            return false;
        }
        if (this.idTipoCaracteristica != other.idTipoCaracteristica) {
            return false;
        }
        if (this.idComprador != other.idComprador) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CaracteristicaPersonaPK[ id=" + id + ", idCaracteristica=" + idCaracteristica + ", idTipoCaracteristica=" + idTipoCaracteristica + ", idComprador=" + idComprador + " ]";
    }
    
}
