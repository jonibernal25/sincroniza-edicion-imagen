/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.trans;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "usuario_software", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioSoftware.findAll", query = "SELECT u FROM UsuarioSoftware u"),
    @NamedQuery(name = "UsuarioSoftware.findByIdUsuario", query = "SELECT u FROM UsuarioSoftware u WHERE u.usuarioSoftwarePK.idUsuario = :idUsuario"),
    @NamedQuery(name = "UsuarioSoftware.findByIdSoftware", query = "SELECT u FROM UsuarioSoftware u WHERE u.usuarioSoftwarePK.idSoftware = :idSoftware")})
public class UsuarioSoftware implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioSoftwarePK usuarioSoftwarePK;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;

    public UsuarioSoftware() {
    }

    public UsuarioSoftware(UsuarioSoftwarePK usuarioSoftwarePK) {
        this.usuarioSoftwarePK = usuarioSoftwarePK;
    }

    public UsuarioSoftware(int idUsuario, int idSoftware) {
        this.usuarioSoftwarePK = new UsuarioSoftwarePK(idUsuario, idSoftware);
    }

    public UsuarioSoftwarePK getUsuarioSoftwarePK() {
        return usuarioSoftwarePK;
    }

    public void setUsuarioSoftwarePK(UsuarioSoftwarePK usuarioSoftwarePK) {
        this.usuarioSoftwarePK = usuarioSoftwarePK;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioSoftwarePK != null ? usuarioSoftwarePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioSoftware)) {
            return false;
        }
        UsuarioSoftware other = (UsuarioSoftware) object;
        if ((this.usuarioSoftwarePK == null && other.usuarioSoftwarePK != null) || (this.usuarioSoftwarePK != null && !this.usuarioSoftwarePK.equals(other.usuarioSoftwarePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.trans.UsuarioSoftware[ usuarioSoftwarePK=" + usuarioSoftwarePK + " ]";
    }
    
}
