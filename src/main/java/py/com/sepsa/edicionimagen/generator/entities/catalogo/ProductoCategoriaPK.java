/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ProductoCategoriaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;
    @Basic(optional = false)
    @Column(name = "id_tipo_categoria")
    private int idTipoCategoria;

    public ProductoCategoriaPK() {
    }

    public ProductoCategoriaPK(int idProducto, int idTipoCategoria) {
        this.idProducto = idProducto;
        this.idTipoCategoria = idTipoCategoria;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdTipoCategoria() {
        return idTipoCategoria;
    }

    public void setIdTipoCategoria(int idTipoCategoria) {
        this.idTipoCategoria = idTipoCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProducto;
        hash += (int) idTipoCategoria;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoCategoriaPK)) {
            return false;
        }
        ProductoCategoriaPK other = (ProductoCategoriaPK) object;
        if (this.idProducto != other.idProducto) {
            return false;
        }
        if (this.idTipoCategoria != other.idTipoCategoria) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProductoCategoriaPK[ idProducto=" + idProducto + ", idTipoCategoria=" + idTipoCategoria + " ]";
    }
    
}
