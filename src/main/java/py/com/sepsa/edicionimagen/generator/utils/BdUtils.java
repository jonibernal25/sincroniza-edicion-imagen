/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.EdicionImagen;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.EstadoCarga;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.Etiqueta;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.HistoricoEdicionImagen;
import py.com.sepsa.edicionimagen.generator.entities.info.Local;
import py.com.sepsa.edicionimagen.generator.entities.info.Persona;
import py.com.sepsa.edicionimagen.generator.entities.info.Producto;
import py.com.sepsa.edicionimagen.generator.entities.trans.Usuario;


/**
 *
 * @author Rubén Ortiz
 */
public class BdUtils {

    /**
     * Fábrica del manejador de entidades
     */
    private EntityManagerFactory emfSepsa = null;
    
    
    /**
     * Obtiene la descripcion de un local
     * @param idLocal Identificador de local
     * @param idPersona Identificador de persona
     * @return Descripcion de local
     */
    public Local findLocal(Integer idLocal, Integer idPersona) {
        
        Local result = null;
        
        if(idLocal != null && idPersona != null) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
            Root<Local> root = cq.from(Local.class);
            cq.select(root);

            cq.where(qb.and(qb.equal(root.get("localPK").get("id"), idLocal),
                    qb.equal(root.get("localPK").get("idPersona"), idPersona)));

            javax.persistence.Query q = getEntityManagerSepsa()
                    .createQuery(cq)
                    .setHint("eclipselink.refresh", "true");

            List<Local> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
            
        return result;
    }

    private EntityManager getEntityManagerSepsa() {
        if(emfSepsa == null) {
            emfSepsa = JpaUtil.getEntityManagerFactorySepsa();
        }
        return emfSepsa.createEntityManager();
    }
    
    /*
    * Constructor
     */
    public BdUtils() {}
    
    /**
     * Obtiene el siguiente valor de la secuencia para el identificador de la entidad documento
     * @return Siguiente valor de la secuencia
     */
    public Integer triggerSequence() {
        javax.persistence.Query q = getEntityManagerSepsa().createNativeQuery("select nextval('trans.documento_id_seq')");
        Long nextVal = (Long) q.getSingleResult();
        return nextVal.intValue();
    }
    
    /**
     * Obtiene una etiqueta segun descripción
     * @param descripcion Descripción
     * @return Etiqueta
     */
    public Etiqueta findEtiqueta(String descripcion) {
        Etiqueta result = null;
        if(descripcion != null && !descripcion.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Etiqueta> root = cq.from(Etiqueta.class);
            cq.select(root);
            cq.where(qb.equal(root.get("descripcion"), descripcion));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<Etiqueta> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Obtiene la lista de 
     * @param idEdicionImagen
     * @param etiqueta
     * @return 
     */
    public List<HistoricoEdicionImagen> findHistoricoEdicionImagen(Integer idEdicionImagen, String etiqueta) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<HistoricoEdicionImagen> root = cq.from(HistoricoEdicionImagen.class);
        cq.select(root);
        
        List<Predicate> predList = new ArrayList<>();
        
        if(idEdicionImagen != null) {
            predList.add(qb.equal(root.get("idEdicionImagen").get("id"), idEdicionImagen));
        }
        
        if(etiqueta != null && !etiqueta.trim().isEmpty()) {
            predList.add(qb.equal(root.get("idEtiqueta").get("descripcion"), etiqueta.trim()));
        }
        
        Predicate pred = qb.and();
        
        for (Predicate predicate : predList) {
            pred = qb.and(pred, predicate);
        }
        
        cq.where(pred);
        Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
        List<HistoricoEdicionImagen> lista = q.getResultList();

        return lista;
    }
    
    /**
     * Obtiene la lista de imagen pendiente de sincronizacion
     * @param first Primer resultado a retornar
     * @param max Cantidad máxima de resultado
     * @return Lista
     */
    public List<EdicionImagen> findPendienteSincronizacion(Integer first, Integer max) {

        String jpql = "select * from catalogo.edicion_imagen ei"
                + " where ei.id in ((SELECT hei.id_edicion_imagen"
                + " FROM catalogo.edicion_imagen ei left join "
                + " catalogo.historico_edicion_imagen hei on hei.id = ei.id_historico_edicion_imagen"
                + " WHERE hei.id_etiqueta = 17)"
                + " except"
                + " (SELECT hei.id_edicion_imagen FROM catalogo.historico_edicion_imagen hei WHERE hei.id_etiqueta = 21))"
                + " limit " + max + " offset " + first;
        Query q = getEntityManagerSepsa()
                .createNativeQuery(jpql, EdicionImagen.class)
                .setHint("eclipselink.refresh", "true");
        
        List<EdicionImagen> lista = q.getResultList();

        return lista;
    }
    
    public int nextValueProductoImagen() {

        String jpql = "select nextval('info.producto_imagen_id_seq');";
        Query q = getEntityManagerSepsa()
                .createNativeQuery(jpql);
        
        Object object = q.getSingleResult();

        int result = ((Long)object).intValue();
        
        return result;
    }
    
    /**
     * Obtiene la lista de imagen pendiente de sincronizacion
     * @param first Primer resultado a retornar
     * @param max Cantidad máxima de resultado
     * @return Lista
     */
    public List<HistoricoEdicionImagen> findPendienteSincronizacionAlt(Integer first, Integer max) {

        String jpql = "(SELECT hei.* "
                + " FROM catalogo.edicion_imagen ei left join "
                + " catalogo.historico_edicion_imagen hei on hei.id = ei.id_historico_edicion_imagen"
                + " WHERE hei.id_etiqueta = 17)"
                + " except"
                + " (SELECT hei.* FROM catalogo.historico_edicion_imagen hei WHERE hei.id_etiqueta = 21)"
                + " limit " + max + " offset " + first;
        Query q = getEntityManagerSepsa()
                .createNativeQuery(jpql, HistoricoEdicionImagen.class)
                .setHint("eclipselink.refresh", "true");
        
        List<HistoricoEdicionImagen> lista = q.getResultList();

        return lista;
    }
    
    /**
     * Obtiene un historico edicion imagen segun filtro
     * @param hash Hash
     * @return HistoricoEdicionImagen
     */
    public HistoricoEdicionImagen findHistoricoEdicionImagen(String hash) {
        HistoricoEdicionImagen result = null;
        if(hash != null && !hash.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<HistoricoEdicionImagen> root = cq.from(HistoricoEdicionImagen.class);
            cq.select(root);
            cq.where(qb.equal(root.get("hash"), hash));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<HistoricoEdicionImagen> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Obtiene una etiqueta segun descripción
     * @param codigoGtin Código GTIN
     * @param ruc Ruc
     * @return Etiqueta
     */
    public List<Producto> findProducto(String codigoGtin, String ruc) {
        List<Producto> result = new ArrayList<>();
        if(codigoGtin != null && !codigoGtin.trim().isEmpty()
                && ruc != null && !ruc.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Producto> root = cq.from(Producto.class);
            Root<EstadoCarga> estadoCarga = cq.from(EstadoCarga.class);
            Join<Producto, Persona> p = root.join("idPersona");
            
            cq.select(root).distinct(true);
            cq.where(qb.and(qb.equal(root.get("id"), estadoCarga.get("estadoCargaPK").get("idProducto")),
                    qb.equal(root.get("codigoGtin"), codigoGtin),
                    qb.equal(p.get("ruc"), ruc)));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            result = q.getResultList();
        }
        return result;
    }
    
    /**
     * Obtiene un producto por codigo gtin 
     * @param codigoGtin Código gtin del producto
     * @return Producto
     */
    public Producto findProducto(String codigoGtin) {
        Producto result = null;
        if(codigoGtin != null && !codigoGtin.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Producto> root = cq.from(Producto.class);
            Root<EstadoCarga> estadoCarga = cq.from(EstadoCarga.class);
            
            cq.select(root).distinct(true);
            cq.where(qb.and(qb.equal(root.get("id"), estadoCarga.get("estadoCargaPK").get("idProducto")),
                    qb.equal(root.get("codigoGtin"), codigoGtin)));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            
            List<Producto> list = q.getResultList();
        
            result = list == null || list.isEmpty()
                ? null
                : list.get(0);
        }
        return result;
    }
    
    /**
     * Obtiene un producto por identificador 
     * @param idProducto Identificador de producto
     * @return Producto
     */
    public Producto findProducto(Integer idProducto) {

            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Producto> root = cq.from(Producto.class);
            
        cq.select(root);
        
        cq.where(qb.equal(root.get("id"), idProducto));
        
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<Producto> list = q.getResultList();
        
        Producto result = list == null || list.isEmpty()
                ? null
                : list.get(0);
        return result;
    }
    
    /**
     * Obtiene un usuario
     * @param usuario Usuario
     * @return Usuario
     */
    public Usuario findUsuario(String usuario) {
        Usuario result = null;
        if(usuario != null && !usuario.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Usuario> root = cq.from(Usuario.class);
            cq.select(root);
            cq.where(qb.equal(root.get("usuario"), usuario));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<Usuario> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Edita una instancia de documento
     * @param object Objeto
     * @return 
     */
    public <T> boolean edit(T object) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param object Objeto
     * @return 
     */
    public <T> boolean create(T object) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param documento Documento
     * @return 
     */
    public Object find(Class clazz, Object id) {
        Object t = null;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            t = em.find(clazz, id);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return t;
    }
}
