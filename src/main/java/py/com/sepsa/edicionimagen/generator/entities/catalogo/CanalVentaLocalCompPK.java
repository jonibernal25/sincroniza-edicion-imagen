/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class CanalVentaLocalCompPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_proveedor")
    private int idProveedor;
    @Basic(optional = false)
    @Column(name = "id_local_comprador")
    private int idLocalComprador;
    @Basic(optional = false)
    @Column(name = "id_comprador")
    private int idComprador;
    @Basic(optional = false)
    @Column(name = "id_tipo_canal")
    private int idTipoCanal;

    public CanalVentaLocalCompPK() {
    }

    public CanalVentaLocalCompPK(int idProveedor, int idLocalComprador, int idComprador, int idTipoCanal) {
        this.idProveedor = idProveedor;
        this.idLocalComprador = idLocalComprador;
        this.idComprador = idComprador;
        this.idTipoCanal = idTipoCanal;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdLocalComprador() {
        return idLocalComprador;
    }

    public void setIdLocalComprador(int idLocalComprador) {
        this.idLocalComprador = idLocalComprador;
    }

    public int getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(int idComprador) {
        this.idComprador = idComprador;
    }

    public int getIdTipoCanal() {
        return idTipoCanal;
    }

    public void setIdTipoCanal(int idTipoCanal) {
        this.idTipoCanal = idTipoCanal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProveedor;
        hash += (int) idLocalComprador;
        hash += (int) idComprador;
        hash += (int) idTipoCanal;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CanalVentaLocalCompPK)) {
            return false;
        }
        CanalVentaLocalCompPK other = (CanalVentaLocalCompPK) object;
        if (this.idProveedor != other.idProveedor) {
            return false;
        }
        if (this.idLocalComprador != other.idLocalComprador) {
            return false;
        }
        if (this.idComprador != other.idComprador) {
            return false;
        }
        if (this.idTipoCanal != other.idTipoCanal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CanalVentaLocalCompPK[ idProveedor=" + idProveedor + ", idLocalComprador=" + idLocalComprador + ", idComprador=" + idComprador + ", idTipoCanal=" + idTipoCanal + " ]";
    }
    
}
