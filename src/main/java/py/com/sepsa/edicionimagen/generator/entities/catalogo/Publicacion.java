/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "publicacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Publicacion.findAll", query = "SELECT p FROM Publicacion p"),
    @NamedQuery(name = "Publicacion.findById", query = "SELECT p FROM Publicacion p WHERE p.id = :id"),
    @NamedQuery(name = "Publicacion.findByIdProducto", query = "SELECT p FROM Publicacion p WHERE p.idProducto = :idProducto"),
    @NamedQuery(name = "Publicacion.findByFechaRegistro", query = "SELECT p FROM Publicacion p WHERE p.fechaRegistro = :fechaRegistro"),
    @NamedQuery(name = "Publicacion.findByFechaVigencia", query = "SELECT p FROM Publicacion p WHERE p.fechaVigencia = :fechaVigencia"),
    @NamedQuery(name = "Publicacion.findByIdUsuario", query = "SELECT p FROM Publicacion p WHERE p.idUsuario = :idUsuario"),
    @NamedQuery(name = "Publicacion.findByFinOperacion", query = "SELECT p FROM Publicacion p WHERE p.finOperacion = :finOperacion")})
public class Publicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_producto")
    private Integer idProducto;
    @Basic(optional = false)
    @Column(name = "fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "fecha_vigencia")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVigencia;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Column(name = "fin_operacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finOperacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "publicacion")
    private Collection<RespuestaPublicacion> respuestaPublicacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "publicacion")
    private Collection<DetallePublicacion> detallePublicacionCollection;
    @JoinColumn(name = "id_operacion", referencedColumnName = "id")
    @ManyToOne
    private OperacionCatalogo idOperacion;
    @JoinColumn(name = "id_tipo_carga", referencedColumnName = "id")
    @ManyToOne
    private TipoCarga idTipoCarga;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPublicacion")
    private Collection<HistoricoProductoCaracteristica> historicoProductoCaracteristicaCollection;

    public Publicacion() {
    }

    public Publicacion(Integer id) {
        this.id = id;
    }

    public Publicacion(Integer id, Date fechaRegistro) {
        this.id = id;
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaVigencia() {
        return fechaVigencia;
    }

    public void setFechaVigencia(Date fechaVigencia) {
        this.fechaVigencia = fechaVigencia;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFinOperacion() {
        return finOperacion;
    }

    public void setFinOperacion(Date finOperacion) {
        this.finOperacion = finOperacion;
    }

    @XmlTransient
    public Collection<RespuestaPublicacion> getRespuestaPublicacionCollection() {
        return respuestaPublicacionCollection;
    }

    public void setRespuestaPublicacionCollection(Collection<RespuestaPublicacion> respuestaPublicacionCollection) {
        this.respuestaPublicacionCollection = respuestaPublicacionCollection;
    }

    @XmlTransient
    public Collection<DetallePublicacion> getDetallePublicacionCollection() {
        return detallePublicacionCollection;
    }

    public void setDetallePublicacionCollection(Collection<DetallePublicacion> detallePublicacionCollection) {
        this.detallePublicacionCollection = detallePublicacionCollection;
    }

    public OperacionCatalogo getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(OperacionCatalogo idOperacion) {
        this.idOperacion = idOperacion;
    }

    public TipoCarga getIdTipoCarga() {
        return idTipoCarga;
    }

    public void setIdTipoCarga(TipoCarga idTipoCarga) {
        this.idTipoCarga = idTipoCarga;
    }

    @XmlTransient
    public Collection<HistoricoProductoCaracteristica> getHistoricoProductoCaracteristicaCollection() {
        return historicoProductoCaracteristicaCollection;
    }

    public void setHistoricoProductoCaracteristicaCollection(Collection<HistoricoProductoCaracteristica> historicoProductoCaracteristicaCollection) {
        this.historicoProductoCaracteristicaCollection = historicoProductoCaracteristicaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Publicacion)) {
            return false;
        }
        Publicacion other = (Publicacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Publicacion[ id=" + id + " ]";
    }
    
}
