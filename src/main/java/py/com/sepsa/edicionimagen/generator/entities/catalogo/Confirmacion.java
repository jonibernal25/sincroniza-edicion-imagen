/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "confirmacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Confirmacion.findAll", query = "SELECT c FROM Confirmacion c"),
    @NamedQuery(name = "Confirmacion.findById", query = "SELECT c FROM Confirmacion c WHERE c.id = :id"),
    @NamedQuery(name = "Confirmacion.findByIdDocumento", query = "SELECT c FROM Confirmacion c WHERE c.idDocumento = :idDocumento"),
    @NamedQuery(name = "Confirmacion.findByIdTipoDocumento", query = "SELECT c FROM Confirmacion c WHERE c.idTipoDocumento = :idTipoDocumento")})
public class Confirmacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_documento")
    private int idDocumento;
    @Basic(optional = false)
    @Column(name = "id_tipo_documento")
    private int idTipoDocumento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConfirmacion")
    private Collection<DetalleConfirmacion> detalleConfirmacionCollection;
    @OneToMany(mappedBy = "idConfirmacion")
    private Collection<UsuarioMensaje> usuarioMensajeCollection;

    public Confirmacion() {
    }

    public Confirmacion(Integer id) {
        this.id = id;
    }

    public Confirmacion(Integer id, int idDocumento, int idTipoDocumento) {
        this.id = id;
        this.idDocumento = idDocumento;
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    @XmlTransient
    public Collection<DetalleConfirmacion> getDetalleConfirmacionCollection() {
        return detalleConfirmacionCollection;
    }

    public void setDetalleConfirmacionCollection(Collection<DetalleConfirmacion> detalleConfirmacionCollection) {
        this.detalleConfirmacionCollection = detalleConfirmacionCollection;
    }

    @XmlTransient
    public Collection<UsuarioMensaje> getUsuarioMensajeCollection() {
        return usuarioMensajeCollection;
    }

    public void setUsuarioMensajeCollection(Collection<UsuarioMensaje> usuarioMensajeCollection) {
        this.usuarioMensajeCollection = usuarioMensajeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Confirmacion)) {
            return false;
        }
        Confirmacion other = (Confirmacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Confirmacion[ id=" + id + " ]";
    }
    
}
