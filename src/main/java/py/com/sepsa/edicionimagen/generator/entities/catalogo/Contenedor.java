/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "contenedor", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contenedor.findAll", query = "SELECT c FROM Contenedor c"),
    @NamedQuery(name = "Contenedor.findByIdContenedor", query = "SELECT c FROM Contenedor c WHERE c.contenedorPK.idContenedor = :idContenedor"),
    @NamedQuery(name = "Contenedor.findByIdContenido", query = "SELECT c FROM Contenedor c WHERE c.contenedorPK.idContenido = :idContenido"),
    @NamedQuery(name = "Contenedor.findByCantidad", query = "SELECT c FROM Contenedor c WHERE c.cantidad = :cantidad")})
public class Contenedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContenedorPK contenedorPK;
    @Basic(optional = false)
    @Column(name = "cantidad")
    private BigInteger cantidad;

    public Contenedor() {
    }

    public Contenedor(ContenedorPK contenedorPK) {
        this.contenedorPK = contenedorPK;
    }

    public Contenedor(ContenedorPK contenedorPK, BigInteger cantidad) {
        this.contenedorPK = contenedorPK;
        this.cantidad = cantidad;
    }

    public Contenedor(int idContenedor, int idContenido) {
        this.contenedorPK = new ContenedorPK(idContenedor, idContenido);
    }

    public ContenedorPK getContenedorPK() {
        return contenedorPK;
    }

    public void setContenedorPK(ContenedorPK contenedorPK) {
        this.contenedorPK = contenedorPK;
    }

    public BigInteger getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigInteger cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contenedorPK != null ? contenedorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contenedor)) {
            return false;
        }
        Contenedor other = (Contenedor) object;
        if ((this.contenedorPK == null && other.contenedorPK != null) || (this.contenedorPK != null && !this.contenedorPK.equals(other.contenedorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Contenedor[ contenedorPK=" + contenedorPK + " ]";
    }
    
}
