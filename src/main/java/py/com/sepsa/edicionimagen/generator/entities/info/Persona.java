/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.info;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "persona", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p"),
    @NamedQuery(name = "Persona.findById", query = "SELECT p FROM Persona p WHERE p.id = :id"),
    @NamedQuery(name = "Persona.findByEstado", query = "SELECT p FROM Persona p WHERE p.estado = :estado"),
    @NamedQuery(name = "Persona.findByRuc", query = "SELECT p FROM Persona p WHERE p.ruc = :ruc"),
    @NamedQuery(name = "Persona.findByDvRuc", query = "SELECT p FROM Persona p WHERE p.dvRuc = :dvRuc")})
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "ruc")
    private String ruc;
    @Column(name = "dv_ruc")
    private BigInteger dvRuc;
    @JoinTable(name = "persona_telefono", joinColumns = {
        @JoinColumn(name = "id_persona", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_telefono", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Telefono> telefonoCollection;
    @ManyToMany(mappedBy = "personaCollection")
    private Collection<Email> emailCollection;
    @JoinTable(name = "persona_software_adicional", joinColumns = {
        @JoinColumn(name = "id_persona", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_software_adicional", referencedColumnName = "id")})
    @ManyToMany
    private Collection<SoftwareAdicional> softwareAdicionalCollection;
    @ManyToMany(mappedBy = "personaCollection")
    private Collection<Contacto> contactoCollection;
    @ManyToMany(mappedBy = "personaCollection")
    private Collection<CategoriaEmpresa> categoriaEmpresaCollection;
    @JoinTable(name = "rubro_persona", joinColumns = {
        @JoinColumn(name = "id_persona", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_rubro", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Rubro> rubroCollection;
    @OneToMany(mappedBy = "idPersona")
    private Collection<CuentaEntidadFinanciera> cuentaEntidadFinancieraCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPersona")
    private Collection<Producto> productoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona")
    private Collection<Local> localCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPersona")
    private Collection<Marca> marcaCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "persona")
    private EntidadFinanciera entidadFinanciera;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona")
    private Collection<Tarjeta> tarjetaCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "persona")
    private Contacto contacto;
    @JoinColumn(name = "id_direccion", referencedColumnName = "id")
    @ManyToOne
    private Direccion idDireccion;
    @JoinColumn(name = "id_tipo_persona", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoPersona idTipoPersona;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "persona")
    private PersonaJuridica personaJuridica;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona")
    private Collection<CompradorPromocion> compradorPromocionCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "persona")
    private PersonaFisica personaFisica;

    public Persona() {
    }

    public Persona(Integer id) {
        this.id = id;
    }

    public Persona(Integer id, Character estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public BigInteger getDvRuc() {
        return dvRuc;
    }

    public void setDvRuc(BigInteger dvRuc) {
        this.dvRuc = dvRuc;
    }

    @XmlTransient
    public Collection<Telefono> getTelefonoCollection() {
        return telefonoCollection;
    }

    public void setTelefonoCollection(Collection<Telefono> telefonoCollection) {
        this.telefonoCollection = telefonoCollection;
    }

    @XmlTransient
    public Collection<Email> getEmailCollection() {
        return emailCollection;
    }

    public void setEmailCollection(Collection<Email> emailCollection) {
        this.emailCollection = emailCollection;
    }

    @XmlTransient
    public Collection<SoftwareAdicional> getSoftwareAdicionalCollection() {
        return softwareAdicionalCollection;
    }

    public void setSoftwareAdicionalCollection(Collection<SoftwareAdicional> softwareAdicionalCollection) {
        this.softwareAdicionalCollection = softwareAdicionalCollection;
    }

    @XmlTransient
    public Collection<Contacto> getContactoCollection() {
        return contactoCollection;
    }

    public void setContactoCollection(Collection<Contacto> contactoCollection) {
        this.contactoCollection = contactoCollection;
    }

    @XmlTransient
    public Collection<CategoriaEmpresa> getCategoriaEmpresaCollection() {
        return categoriaEmpresaCollection;
    }

    public void setCategoriaEmpresaCollection(Collection<CategoriaEmpresa> categoriaEmpresaCollection) {
        this.categoriaEmpresaCollection = categoriaEmpresaCollection;
    }

    @XmlTransient
    public Collection<Rubro> getRubroCollection() {
        return rubroCollection;
    }

    public void setRubroCollection(Collection<Rubro> rubroCollection) {
        this.rubroCollection = rubroCollection;
    }

    @XmlTransient
    public Collection<CuentaEntidadFinanciera> getCuentaEntidadFinancieraCollection() {
        return cuentaEntidadFinancieraCollection;
    }

    public void setCuentaEntidadFinancieraCollection(Collection<CuentaEntidadFinanciera> cuentaEntidadFinancieraCollection) {
        this.cuentaEntidadFinancieraCollection = cuentaEntidadFinancieraCollection;
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

    @XmlTransient
    public Collection<Local> getLocalCollection() {
        return localCollection;
    }

    public void setLocalCollection(Collection<Local> localCollection) {
        this.localCollection = localCollection;
    }

    @XmlTransient
    public Collection<Marca> getMarcaCollection() {
        return marcaCollection;
    }

    public void setMarcaCollection(Collection<Marca> marcaCollection) {
        this.marcaCollection = marcaCollection;
    }

    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    @XmlTransient
    public Collection<Tarjeta> getTarjetaCollection() {
        return tarjetaCollection;
    }

    public void setTarjetaCollection(Collection<Tarjeta> tarjetaCollection) {
        this.tarjetaCollection = tarjetaCollection;
    }

    public Contacto getContacto() {
        return contacto;
    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    public Direccion getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Direccion idDireccion) {
        this.idDireccion = idDireccion;
    }

    public TipoPersona getIdTipoPersona() {
        return idTipoPersona;
    }

    public void setIdTipoPersona(TipoPersona idTipoPersona) {
        this.idTipoPersona = idTipoPersona;
    }

    public PersonaJuridica getPersonaJuridica() {
        return personaJuridica;
    }

    public void setPersonaJuridica(PersonaJuridica personaJuridica) {
        this.personaJuridica = personaJuridica;
    }

    @XmlTransient
    public Collection<CompradorPromocion> getCompradorPromocionCollection() {
        return compradorPromocionCollection;
    }

    public void setCompradorPromocionCollection(Collection<CompradorPromocion> compradorPromocionCollection) {
        this.compradorPromocionCollection = compradorPromocionCollection;
    }

    public PersonaFisica getPersonaFisica() {
        return personaFisica;
    }

    public void setPersonaFisica(PersonaFisica personaFisica) {
        this.personaFisica = personaFisica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.info.Persona[ id=" + id + " ]";
    }
    
}
