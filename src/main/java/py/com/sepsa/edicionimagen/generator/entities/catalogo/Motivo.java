/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "motivo", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Motivo.findAll", query = "SELECT m FROM Motivo m"),
    @NamedQuery(name = "Motivo.findById", query = "SELECT m FROM Motivo m WHERE m.id = :id"),
    @NamedQuery(name = "Motivo.findByDescripcion", query = "SELECT m FROM Motivo m WHERE m.descripcion = :descripcion"),
    @NamedQuery(name = "Motivo.findByEtiqueta", query = "SELECT m FROM Motivo m WHERE m.etiqueta = :etiqueta")})
public class Motivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "etiqueta")
    private String etiqueta;
    @OneToMany(mappedBy = "idMotivo")
    private Collection<RespuestaPublicacion> respuestaPublicacionCollection;

    public Motivo() {
    }

    public Motivo(Integer id) {
        this.id = id;
    }

    public Motivo(Integer id, String descripcion, String etiqueta) {
        this.id = id;
        this.descripcion = descripcion;
        this.etiqueta = etiqueta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    @XmlTransient
    public Collection<RespuestaPublicacion> getRespuestaPublicacionCollection() {
        return respuestaPublicacionCollection;
    }

    public void setRespuestaPublicacionCollection(Collection<RespuestaPublicacion> respuestaPublicacionCollection) {
        this.respuestaPublicacionCollection = respuestaPublicacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Motivo)) {
            return false;
        }
        Motivo other = (Motivo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Motivo[ id=" + id + " ]";
    }
    
}
