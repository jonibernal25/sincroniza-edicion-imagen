/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "conversion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conversion.findAll", query = "SELECT c FROM Conversion c"),
    @NamedQuery(name = "Conversion.findByIdMetricaOrigen", query = "SELECT c FROM Conversion c WHERE c.conversionPK.idMetricaOrigen = :idMetricaOrigen"),
    @NamedQuery(name = "Conversion.findByIdMetricaDestino", query = "SELECT c FROM Conversion c WHERE c.conversionPK.idMetricaDestino = :idMetricaDestino"),
    @NamedQuery(name = "Conversion.findByValor", query = "SELECT c FROM Conversion c WHERE c.valor = :valor")})
public class Conversion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConversionPK conversionPK;
    @Column(name = "valor")
    private String valor;
    @JoinColumn(name = "id_metrica_origen", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Metrica metrica;
    @JoinColumn(name = "id_metrica_destino", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Metrica metrica1;

    public Conversion() {
    }

    public Conversion(ConversionPK conversionPK) {
        this.conversionPK = conversionPK;
    }

    public Conversion(int idMetricaOrigen, int idMetricaDestino) {
        this.conversionPK = new ConversionPK(idMetricaOrigen, idMetricaDestino);
    }

    public ConversionPK getConversionPK() {
        return conversionPK;
    }

    public void setConversionPK(ConversionPK conversionPK) {
        this.conversionPK = conversionPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    public Metrica getMetrica1() {
        return metrica1;
    }

    public void setMetrica1(Metrica metrica1) {
        this.metrica1 = metrica1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conversionPK != null ? conversionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conversion)) {
            return false;
        }
        Conversion other = (Conversion) object;
        if ((this.conversionPK == null && other.conversionPK != null) || (this.conversionPK != null && !this.conversionPK.equals(other.conversionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Conversion[ conversionPK=" + conversionPK + " ]";
    }
    
}
