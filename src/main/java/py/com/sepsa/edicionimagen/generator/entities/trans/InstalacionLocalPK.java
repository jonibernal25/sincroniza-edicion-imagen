/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class InstalacionLocalPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_instalacion")
    private int idInstalacion;
    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @Column(name = "id_local")
    private int idLocal;

    public InstalacionLocalPK() {
    }

    public InstalacionLocalPK(int idInstalacion, int idPersona, int idLocal) {
        this.idInstalacion = idInstalacion;
        this.idPersona = idPersona;
        this.idLocal = idLocal;
    }

    public int getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(int idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idInstalacion;
        hash += (int) idPersona;
        hash += (int) idLocal;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstalacionLocalPK)) {
            return false;
        }
        InstalacionLocalPK other = (InstalacionLocalPK) object;
        if (this.idInstalacion != other.idInstalacion) {
            return false;
        }
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idLocal != other.idLocal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.trans.InstalacionLocalPK[ idInstalacion=" + idInstalacion + ", idPersona=" + idPersona + ", idLocal=" + idLocal + " ]";
    }
    
}
