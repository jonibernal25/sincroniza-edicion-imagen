/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "detalle_publicacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetallePublicacion.findAll", query = "SELECT d FROM DetallePublicacion d"),
    @NamedQuery(name = "DetallePublicacion.findByIdPublicacion", query = "SELECT d FROM DetallePublicacion d WHERE d.detallePublicacionPK.idPublicacion = :idPublicacion"),
    @NamedQuery(name = "DetallePublicacion.findByIdTipoDocumento", query = "SELECT d FROM DetallePublicacion d WHERE d.detallePublicacionPK.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "DetallePublicacion.findByIdDocumento", query = "SELECT d FROM DetallePublicacion d WHERE d.detallePublicacionPK.idDocumento = :idDocumento"),
    @NamedQuery(name = "DetallePublicacion.findBySourceGtin", query = "SELECT d FROM DetallePublicacion d WHERE d.sourceGtin = :sourceGtin"),
    @NamedQuery(name = "DetallePublicacion.findBySourceGln", query = "SELECT d FROM DetallePublicacion d WHERE d.sourceGln = :sourceGln"),
    @NamedQuery(name = "DetallePublicacion.findByRecipientGln", query = "SELECT d FROM DetallePublicacion d WHERE d.recipientGln = :recipientGln")})
public class DetallePublicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetallePublicacionPK detallePublicacionPK;
    @Basic(optional = false)
    @Column(name = "source_gtin")
    private String sourceGtin;
    @Basic(optional = false)
    @Column(name = "source_gln")
    private BigInteger sourceGln;
    @Column(name = "recipient_gln")
    private BigInteger recipientGln;
    @JoinColumn(name = "id_codigo_pais", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CodigoPais idCodigoPais;
    @JoinColumn(name = "id_codigo_subdivision", referencedColumnName = "id")
    @ManyToOne
    private CodigoSubdivision idCodigoSubdivision;
    @JoinColumn(name = "id_publicacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Publicacion publicacion;

    public DetallePublicacion() {
    }

    public DetallePublicacion(DetallePublicacionPK detallePublicacionPK) {
        this.detallePublicacionPK = detallePublicacionPK;
    }

    public DetallePublicacion(DetallePublicacionPK detallePublicacionPK, String sourceGtin, BigInteger sourceGln) {
        this.detallePublicacionPK = detallePublicacionPK;
        this.sourceGtin = sourceGtin;
        this.sourceGln = sourceGln;
    }

    public DetallePublicacion(int idPublicacion, int idTipoDocumento, int idDocumento) {
        this.detallePublicacionPK = new DetallePublicacionPK(idPublicacion, idTipoDocumento, idDocumento);
    }

    public DetallePublicacionPK getDetallePublicacionPK() {
        return detallePublicacionPK;
    }

    public void setDetallePublicacionPK(DetallePublicacionPK detallePublicacionPK) {
        this.detallePublicacionPK = detallePublicacionPK;
    }

    public String getSourceGtin() {
        return sourceGtin;
    }

    public void setSourceGtin(String sourceGtin) {
        this.sourceGtin = sourceGtin;
    }

    public BigInteger getSourceGln() {
        return sourceGln;
    }

    public void setSourceGln(BigInteger sourceGln) {
        this.sourceGln = sourceGln;
    }

    public BigInteger getRecipientGln() {
        return recipientGln;
    }

    public void setRecipientGln(BigInteger recipientGln) {
        this.recipientGln = recipientGln;
    }

    public CodigoPais getIdCodigoPais() {
        return idCodigoPais;
    }

    public void setIdCodigoPais(CodigoPais idCodigoPais) {
        this.idCodigoPais = idCodigoPais;
    }

    public CodigoSubdivision getIdCodigoSubdivision() {
        return idCodigoSubdivision;
    }

    public void setIdCodigoSubdivision(CodigoSubdivision idCodigoSubdivision) {
        this.idCodigoSubdivision = idCodigoSubdivision;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detallePublicacionPK != null ? detallePublicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallePublicacion)) {
            return false;
        }
        DetallePublicacion other = (DetallePublicacion) object;
        if ((this.detallePublicacionPK == null && other.detallePublicacionPK != null) || (this.detallePublicacionPK != null && !this.detallePublicacionPK.equals(other.detallePublicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.DetallePublicacion[ detallePublicacionPK=" + detallePublicacionPK + " ]";
    }
    
}
