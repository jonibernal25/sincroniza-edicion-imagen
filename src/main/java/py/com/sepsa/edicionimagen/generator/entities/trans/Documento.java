/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "documento", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documento.findAll", query = "SELECT d FROM Documento d"),
    @NamedQuery(name = "Documento.findById", query = "SELECT d FROM Documento d WHERE d.documentoPK.id = :id"),
    @NamedQuery(name = "Documento.findByIdTipoDocumento", query = "SELECT d FROM Documento d WHERE d.documentoPK.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "Documento.findByIdPersonaOrigen", query = "SELECT d FROM Documento d WHERE d.idPersonaOrigen = :idPersonaOrigen"),
    @NamedQuery(name = "Documento.findByIdLocalOrigen", query = "SELECT d FROM Documento d WHERE d.idLocalOrigen = :idLocalOrigen"),
    @NamedQuery(name = "Documento.findByIdPersonaDestino", query = "SELECT d FROM Documento d WHERE d.idPersonaDestino = :idPersonaDestino"),
    @NamedQuery(name = "Documento.findByIdLocalDestino", query = "SELECT d FROM Documento d WHERE d.idLocalDestino = :idLocalDestino"),
    @NamedQuery(name = "Documento.findByNroDocumento", query = "SELECT d FROM Documento d WHERE d.nroDocumento = :nroDocumento"),
    @NamedQuery(name = "Documento.findByFechaDocumento", query = "SELECT d FROM Documento d WHERE d.fechaDocumento = :fechaDocumento"),
    @NamedQuery(name = "Documento.findByCantidadLineas", query = "SELECT d FROM Documento d WHERE d.cantidadLineas = :cantidadLineas"),
    @NamedQuery(name = "Documento.findByFechaInsercion", query = "SELECT d FROM Documento d WHERE d.fechaInsercion = :fechaInsercion"),
    @NamedQuery(name = "Documento.findByCantidadDescargas", query = "SELECT d FROM Documento d WHERE d.cantidadDescargas = :cantidadDescargas"),
    @NamedQuery(name = "Documento.findByEstado", query = "SELECT d FROM Documento d WHERE d.estado = :estado")})
public class Documento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DocumentoPK documentoPK;
    @Column(name = "id_persona_origen")
    private Integer idPersonaOrigen;
    @Basic(optional = false)
    @Column(name = "id_local_origen")
    private int idLocalOrigen;
    @Column(name = "id_persona_destino")
    private Integer idPersonaDestino;
    @Basic(optional = false)
    @Column(name = "id_local_destino")
    private int idLocalDestino;
    @Basic(optional = false)
    @Column(name = "nro_documento")
    private String nroDocumento;
    @Basic(optional = false)
    @Column(name = "fecha_documento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDocumento;
    @Basic(optional = false)
    @Column(name = "cantidad_lineas")
    private int cantidadLineas;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @Column(name = "cantidad_descargas")
    private int cantidadDescargas;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinTable(name = "cadena_documento", joinColumns = {
        @JoinColumn(name = "id_doc", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_doc", referencedColumnName = "id_tipo_documento")}, inverseJoinColumns = {
        @JoinColumn(name = "id_doc_rel", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_doc_rel", referencedColumnName = "id_tipo_documento")})
    @ManyToMany
    private Collection<Documento> documentoCollection;
    @ManyToMany(mappedBy = "documentoCollection")
    private Collection<Documento> documentoCollection1;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDocumento tipoDocumento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documento")
    private Collection<Operacion> operacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documento")
    private Collection<DocumentoProducto> documentoProductoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documento")
    private Collection<DatoAdicional> datoAdicionalCollection;

    public Documento() {
    }

    public Documento(DocumentoPK documentoPK) {
        this.documentoPK = documentoPK;
    }

    public Documento(DocumentoPK documentoPK, int idLocalOrigen, int idLocalDestino, String nroDocumento, Date fechaDocumento, int cantidadLineas, Date fechaInsercion, int cantidadDescargas, Character estado) {
        this.documentoPK = documentoPK;
        this.idLocalOrigen = idLocalOrigen;
        this.idLocalDestino = idLocalDestino;
        this.nroDocumento = nroDocumento;
        this.fechaDocumento = fechaDocumento;
        this.cantidadLineas = cantidadLineas;
        this.fechaInsercion = fechaInsercion;
        this.cantidadDescargas = cantidadDescargas;
        this.estado = estado;
    }

    public Documento(int id, int idTipoDocumento) {
        this.documentoPK = new DocumentoPK(id, idTipoDocumento);
    }

    public DocumentoPK getDocumentoPK() {
        return documentoPK;
    }

    public void setDocumentoPK(DocumentoPK documentoPK) {
        this.documentoPK = documentoPK;
    }

    public Integer getIdPersonaOrigen() {
        return idPersonaOrigen;
    }

    public void setIdPersonaOrigen(Integer idPersonaOrigen) {
        this.idPersonaOrigen = idPersonaOrigen;
    }

    public int getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(int idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdPersonaDestino() {
        return idPersonaDestino;
    }

    public void setIdPersonaDestino(Integer idPersonaDestino) {
        this.idPersonaDestino = idPersonaDestino;
    }

    public int getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(int idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public int getCantidadLineas() {
        return cantidadLineas;
    }

    public void setCantidadLineas(int cantidadLineas) {
        this.cantidadLineas = cantidadLineas;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public int getCantidadDescargas() {
        return cantidadDescargas;
    }

    public void setCantidadDescargas(int cantidadDescargas) {
        this.cantidadDescargas = cantidadDescargas;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<Documento> getDocumentoCollection() {
        return documentoCollection;
    }

    public void setDocumentoCollection(Collection<Documento> documentoCollection) {
        this.documentoCollection = documentoCollection;
    }

    @XmlTransient
    public Collection<Documento> getDocumentoCollection1() {
        return documentoCollection1;
    }

    public void setDocumentoCollection1(Collection<Documento> documentoCollection1) {
        this.documentoCollection1 = documentoCollection1;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @XmlTransient
    public Collection<Operacion> getOperacionCollection() {
        return operacionCollection;
    }

    public void setOperacionCollection(Collection<Operacion> operacionCollection) {
        this.operacionCollection = operacionCollection;
    }

    @XmlTransient
    public Collection<DocumentoProducto> getDocumentoProductoCollection() {
        return documentoProductoCollection;
    }

    public void setDocumentoProductoCollection(Collection<DocumentoProducto> documentoProductoCollection) {
        this.documentoProductoCollection = documentoProductoCollection;
    }

    @XmlTransient
    public Collection<DatoAdicional> getDatoAdicionalCollection() {
        return datoAdicionalCollection;
    }

    public void setDatoAdicionalCollection(Collection<DatoAdicional> datoAdicionalCollection) {
        this.datoAdicionalCollection = datoAdicionalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentoPK != null ? documentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documento)) {
            return false;
        }
        Documento other = (Documento) object;
        if ((this.documentoPK == null && other.documentoPK != null) || (this.documentoPK != null && !this.documentoPK.equals(other.documentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.trans.Documento[ documentoPK=" + documentoPK + " ]";
    }
    
}
