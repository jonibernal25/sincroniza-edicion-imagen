/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "usuario_area", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioArea.findAll", query = "SELECT u FROM UsuarioArea u"),
    @NamedQuery(name = "UsuarioArea.findByIdUsuario", query = "SELECT u FROM UsuarioArea u WHERE u.usuarioAreaPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "UsuarioArea.findByIdArea", query = "SELECT u FROM UsuarioArea u WHERE u.usuarioAreaPK.idArea = :idArea")})
public class UsuarioArea implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioAreaPK usuarioAreaPK;
    @JoinColumn(name = "id_area", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Area area;

    public UsuarioArea() {
    }

    public UsuarioArea(UsuarioAreaPK usuarioAreaPK) {
        this.usuarioAreaPK = usuarioAreaPK;
    }

    public UsuarioArea(int idUsuario, int idArea) {
        this.usuarioAreaPK = new UsuarioAreaPK(idUsuario, idArea);
    }

    public UsuarioAreaPK getUsuarioAreaPK() {
        return usuarioAreaPK;
    }

    public void setUsuarioAreaPK(UsuarioAreaPK usuarioAreaPK) {
        this.usuarioAreaPK = usuarioAreaPK;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioAreaPK != null ? usuarioAreaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioArea)) {
            return false;
        }
        UsuarioArea other = (UsuarioArea) object;
        if ((this.usuarioAreaPK == null && other.usuarioAreaPK != null) || (this.usuarioAreaPK != null && !this.usuarioAreaPK.equals(other.usuarioAreaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.UsuarioArea[ usuarioAreaPK=" + usuarioAreaPK + " ]";
    }
    
}
