/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "edicion_imagen", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
public class EdicionImagen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private Integer idProducto;
    @JoinColumn(name = "id_historico_edicion_imagen", referencedColumnName = "id")
    @ManyToOne
    private HistoricoEdicionImagen idHistoricoEdicionImagen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEdicionImagen")
    private Collection<HistoricoEdicionImagen> historicoEdicionImagenCollection;

    public EdicionImagen() {
    }

    public EdicionImagen(Integer id) {
        this.id = id;
    }

    public EdicionImagen(Integer id, Integer idProducto) {
        this.id = id;
        this.idProducto = idProducto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public HistoricoEdicionImagen getIdHistoricoEdicionImagen() {
        return idHistoricoEdicionImagen;
    }

    public void setIdHistoricoEdicionImagen(HistoricoEdicionImagen idHistoricoEdicionImagen) {
        this.idHistoricoEdicionImagen = idHistoricoEdicionImagen;
    }

    @XmlTransient
    public Collection<HistoricoEdicionImagen> getHistoricoEdicionImagenCollection() {
        return historicoEdicionImagenCollection;
    }

    public void setHistoricoEdicionImagenCollection(Collection<HistoricoEdicionImagen> historicoEdicionImagenCollection) {
        this.historicoEdicionImagenCollection = historicoEdicionImagenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EdicionImagen)) {
            return false;
        }
        EdicionImagen other = (EdicionImagen) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.EdicionImagen[ id=" + id + " ]";
    }
    
}
