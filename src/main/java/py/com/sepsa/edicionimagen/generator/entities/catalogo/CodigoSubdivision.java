/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "codigo_subdivision", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CodigoSubdivision.findAll", query = "SELECT c FROM CodigoSubdivision c"),
    @NamedQuery(name = "CodigoSubdivision.findById", query = "SELECT c FROM CodigoSubdivision c WHERE c.id = :id"),
    @NamedQuery(name = "CodigoSubdivision.findByNombre", query = "SELECT c FROM CodigoSubdivision c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CodigoSubdivision.findByCodigo", query = "SELECT c FROM CodigoSubdivision c WHERE c.codigo = :codigo")})
public class CodigoSubdivision implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @OneToMany(mappedBy = "idCodigoSubdivision")
    private Collection<Subscripcion> subscripcionCollection;
    @OneToMany(mappedBy = "idCodigoSubdivision")
    private Collection<Sincronizacion> sincronizacionCollection;
    @OneToMany(mappedBy = "idCodigoSubdivision")
    private Collection<CalificadorDescripcionArticulo> calificadorDescripcionArticuloCollection;
    @OneToMany(mappedBy = "idCodigoSubdivision")
    private Collection<DetallePublicacion> detallePublicacionCollection;
    @JoinColumn(name = "id_codigo_pais", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CodigoPais idCodigoPais;

    public CodigoSubdivision() {
    }

    public CodigoSubdivision(Integer id) {
        this.id = id;
    }

    public CodigoSubdivision(Integer id, String nombre, String codigo) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @XmlTransient
    public Collection<Subscripcion> getSubscripcionCollection() {
        return subscripcionCollection;
    }

    public void setSubscripcionCollection(Collection<Subscripcion> subscripcionCollection) {
        this.subscripcionCollection = subscripcionCollection;
    }

    @XmlTransient
    public Collection<Sincronizacion> getSincronizacionCollection() {
        return sincronizacionCollection;
    }

    public void setSincronizacionCollection(Collection<Sincronizacion> sincronizacionCollection) {
        this.sincronizacionCollection = sincronizacionCollection;
    }

    @XmlTransient
    public Collection<CalificadorDescripcionArticulo> getCalificadorDescripcionArticuloCollection() {
        return calificadorDescripcionArticuloCollection;
    }

    public void setCalificadorDescripcionArticuloCollection(Collection<CalificadorDescripcionArticulo> calificadorDescripcionArticuloCollection) {
        this.calificadorDescripcionArticuloCollection = calificadorDescripcionArticuloCollection;
    }

    @XmlTransient
    public Collection<DetallePublicacion> getDetallePublicacionCollection() {
        return detallePublicacionCollection;
    }

    public void setDetallePublicacionCollection(Collection<DetallePublicacion> detallePublicacionCollection) {
        this.detallePublicacionCollection = detallePublicacionCollection;
    }

    public CodigoPais getIdCodigoPais() {
        return idCodigoPais;
    }

    public void setIdCodigoPais(CodigoPais idCodigoPais) {
        this.idCodigoPais = idCodigoPais;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CodigoSubdivision)) {
            return false;
        }
        CodigoSubdivision other = (CodigoSubdivision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CodigoSubdivision[ id=" + id + " ]";
    }
    
}
