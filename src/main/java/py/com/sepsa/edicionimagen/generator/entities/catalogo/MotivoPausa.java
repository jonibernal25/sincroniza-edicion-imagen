/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "motivo_pausa", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MotivoPausa.findAll", query = "SELECT m FROM MotivoPausa m"),
    @NamedQuery(name = "MotivoPausa.findById", query = "SELECT m FROM MotivoPausa m WHERE m.id = :id"),
    @NamedQuery(name = "MotivoPausa.findByDescripcion", query = "SELECT m FROM MotivoPausa m WHERE m.descripcion = :descripcion")})
public class MotivoPausa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idMotivo")
    private Collection<TareaDiariaDetalle> tareaDiariaDetalleCollection;

    public MotivoPausa() {
    }

    public MotivoPausa(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<TareaDiariaDetalle> getTareaDiariaDetalleCollection() {
        return tareaDiariaDetalleCollection;
    }

    public void setTareaDiariaDetalleCollection(Collection<TareaDiariaDetalle> tareaDiariaDetalleCollection) {
        this.tareaDiariaDetalleCollection = tareaDiariaDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MotivoPausa)) {
            return false;
        }
        MotivoPausa other = (MotivoPausa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.MotivoPausa[ id=" + id + " ]";
    }
    
}
