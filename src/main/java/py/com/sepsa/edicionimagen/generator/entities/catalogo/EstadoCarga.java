/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "estado_carga", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoCarga.findAll", query = "SELECT e FROM EstadoCarga e"),
    @NamedQuery(name = "EstadoCarga.findByIdProducto", query = "SELECT e FROM EstadoCarga e WHERE e.estadoCargaPK.idProducto = :idProducto"),
    @NamedQuery(name = "EstadoCarga.findByIdEstadoCarga", query = "SELECT e FROM EstadoCarga e WHERE e.estadoCargaPK.idEstadoCarga = :idEstadoCarga")})
public class EstadoCarga implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EstadoCargaPK estadoCargaPK;
    @JoinColumn(name = "id_estado_carga", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoEstadoCarga tipoEstadoCarga;

    public EstadoCarga() {
    }

    public EstadoCarga(EstadoCargaPK estadoCargaPK) {
        this.estadoCargaPK = estadoCargaPK;
    }

    public EstadoCarga(int idProducto, int idEstadoCarga) {
        this.estadoCargaPK = new EstadoCargaPK(idProducto, idEstadoCarga);
    }

    public EstadoCargaPK getEstadoCargaPK() {
        return estadoCargaPK;
    }

    public void setEstadoCargaPK(EstadoCargaPK estadoCargaPK) {
        this.estadoCargaPK = estadoCargaPK;
    }

    public TipoEstadoCarga getTipoEstadoCarga() {
        return tipoEstadoCarga;
    }

    public void setTipoEstadoCarga(TipoEstadoCarga tipoEstadoCarga) {
        this.tipoEstadoCarga = tipoEstadoCarga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estadoCargaPK != null ? estadoCargaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoCarga)) {
            return false;
        }
        EstadoCarga other = (EstadoCarga) object;
        if ((this.estadoCargaPK == null && other.estadoCargaPK != null) || (this.estadoCargaPK != null && !this.estadoCargaPK.equals(other.estadoCargaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.EstadoCarga[ estadoCargaPK=" + estadoCargaPK + " ]";
    }
    
}
