/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "canal_venta", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CanalVenta.findAll", query = "SELECT c FROM CanalVenta c"),
    @NamedQuery(name = "CanalVenta.findById", query = "SELECT c FROM CanalVenta c WHERE c.id = :id"),
    @NamedQuery(name = "CanalVenta.findByIdProveedor", query = "SELECT c FROM CanalVenta c WHERE c.idProveedor = :idProveedor"),
    @NamedQuery(name = "CanalVenta.findByDescripcion", query = "SELECT c FROM CanalVenta c WHERE c.descripcion = :descripcion")})
public class CanalVenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_proveedor")
    private int idProveedor;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idCanalVenta")
    private Collection<Promocion> promocionCollection;
    @OneToMany(mappedBy = "idCanalVenta")
    private Collection<ValorCaracteristica> valorCaracteristicaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCanal")
    private Collection<CanalVentaLocalComp> canalVentaLocalCompCollection;
    @JoinColumn(name = "id_tipo_canal", referencedColumnName = "id")
    @ManyToOne
    private TipoCanal idTipoCanal;
    @OneToMany(mappedBy = "idCanalVenta")
    private Collection<PrecioProducto> precioProductoCollection;
    @OneToMany(mappedBy = "idCanal")
    private Collection<HistoricoValorCaracteristica> historicoValorCaracteristicaCollection;

    public CanalVenta() {
    }

    public CanalVenta(Integer id) {
        this.id = id;
    }

    public CanalVenta(Integer id, int idProveedor, String descripcion) {
        this.id = id;
        this.idProveedor = idProveedor;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<Promocion> getPromocionCollection() {
        return promocionCollection;
    }

    public void setPromocionCollection(Collection<Promocion> promocionCollection) {
        this.promocionCollection = promocionCollection;
    }

    @XmlTransient
    public Collection<ValorCaracteristica> getValorCaracteristicaCollection() {
        return valorCaracteristicaCollection;
    }

    public void setValorCaracteristicaCollection(Collection<ValorCaracteristica> valorCaracteristicaCollection) {
        this.valorCaracteristicaCollection = valorCaracteristicaCollection;
    }

    @XmlTransient
    public Collection<CanalVentaLocalComp> getCanalVentaLocalCompCollection() {
        return canalVentaLocalCompCollection;
    }

    public void setCanalVentaLocalCompCollection(Collection<CanalVentaLocalComp> canalVentaLocalCompCollection) {
        this.canalVentaLocalCompCollection = canalVentaLocalCompCollection;
    }

    public TipoCanal getIdTipoCanal() {
        return idTipoCanal;
    }

    public void setIdTipoCanal(TipoCanal idTipoCanal) {
        this.idTipoCanal = idTipoCanal;
    }

    @XmlTransient
    public Collection<PrecioProducto> getPrecioProductoCollection() {
        return precioProductoCollection;
    }

    public void setPrecioProductoCollection(Collection<PrecioProducto> precioProductoCollection) {
        this.precioProductoCollection = precioProductoCollection;
    }

    @XmlTransient
    public Collection<HistoricoValorCaracteristica> getHistoricoValorCaracteristicaCollection() {
        return historicoValorCaracteristicaCollection;
    }

    public void setHistoricoValorCaracteristicaCollection(Collection<HistoricoValorCaracteristica> historicoValorCaracteristicaCollection) {
        this.historicoValorCaracteristicaCollection = historicoValorCaracteristicaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CanalVenta)) {
            return false;
        }
        CanalVenta other = (CanalVenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CanalVenta[ id=" + id + " ]";
    }
    
}
