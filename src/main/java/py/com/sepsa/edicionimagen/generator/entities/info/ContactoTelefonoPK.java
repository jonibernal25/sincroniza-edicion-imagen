/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ContactoTelefonoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_contacto")
    private int idContacto;
    @Basic(optional = false)
    @Column(name = "id_telefono")
    private int idTelefono;

    public ContactoTelefonoPK() {
    }

    public ContactoTelefonoPK(int idContacto, int idTelefono) {
        this.idContacto = idContacto;
        this.idTelefono = idTelefono;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    public int getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(int idTelefono) {
        this.idTelefono = idTelefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idContacto;
        hash += (int) idTelefono;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactoTelefonoPK)) {
            return false;
        }
        ContactoTelefonoPK other = (ContactoTelefonoPK) object;
        if (this.idContacto != other.idContacto) {
            return false;
        }
        if (this.idTelefono != other.idTelefono) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.info.ContactoTelefonoPK[ idContacto=" + idContacto + ", idTelefono=" + idTelefono + " ]";
    }
    
}
