/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "canal_venta_local_comp", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CanalVentaLocalComp.findAll", query = "SELECT c FROM CanalVentaLocalComp c"),
    @NamedQuery(name = "CanalVentaLocalComp.findByIdProveedor", query = "SELECT c FROM CanalVentaLocalComp c WHERE c.canalVentaLocalCompPK.idProveedor = :idProveedor"),
    @NamedQuery(name = "CanalVentaLocalComp.findByIdLocalComprador", query = "SELECT c FROM CanalVentaLocalComp c WHERE c.canalVentaLocalCompPK.idLocalComprador = :idLocalComprador"),
    @NamedQuery(name = "CanalVentaLocalComp.findByIdComprador", query = "SELECT c FROM CanalVentaLocalComp c WHERE c.canalVentaLocalCompPK.idComprador = :idComprador"),
    @NamedQuery(name = "CanalVentaLocalComp.findByIdTipoCanal", query = "SELECT c FROM CanalVentaLocalComp c WHERE c.canalVentaLocalCompPK.idTipoCanal = :idTipoCanal")})
public class CanalVentaLocalComp implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CanalVentaLocalCompPK canalVentaLocalCompPK;
    @JoinColumn(name = "id_canal", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CanalVenta idCanal;
    @JoinColumn(name = "id_tipo_canal", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoCanal tipoCanal;

    public CanalVentaLocalComp() {
    }

    public CanalVentaLocalComp(CanalVentaLocalCompPK canalVentaLocalCompPK) {
        this.canalVentaLocalCompPK = canalVentaLocalCompPK;
    }

    public CanalVentaLocalComp(int idProveedor, int idLocalComprador, int idComprador, int idTipoCanal) {
        this.canalVentaLocalCompPK = new CanalVentaLocalCompPK(idProveedor, idLocalComprador, idComprador, idTipoCanal);
    }

    public CanalVentaLocalCompPK getCanalVentaLocalCompPK() {
        return canalVentaLocalCompPK;
    }

    public void setCanalVentaLocalCompPK(CanalVentaLocalCompPK canalVentaLocalCompPK) {
        this.canalVentaLocalCompPK = canalVentaLocalCompPK;
    }

    public CanalVenta getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(CanalVenta idCanal) {
        this.idCanal = idCanal;
    }

    public TipoCanal getTipoCanal() {
        return tipoCanal;
    }

    public void setTipoCanal(TipoCanal tipoCanal) {
        this.tipoCanal = tipoCanal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (canalVentaLocalCompPK != null ? canalVentaLocalCompPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CanalVentaLocalComp)) {
            return false;
        }
        CanalVentaLocalComp other = (CanalVentaLocalComp) object;
        if ((this.canalVentaLocalCompPK == null && other.canalVentaLocalCompPK != null) || (this.canalVentaLocalCompPK != null && !this.canalVentaLocalCompPK.equals(other.canalVentaLocalCompPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CanalVentaLocalComp[ canalVentaLocalCompPK=" + canalVentaLocalCompPK + " ]";
    }
    
}
