/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "dato_logistico", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatoLogistico.findAll", query = "SELECT d FROM DatoLogistico d"),
    @NamedQuery(name = "DatoLogistico.findByCodigoGtin", query = "SELECT d FROM DatoLogistico d WHERE d.codigoGtin = :codigoGtin"),
    @NamedQuery(name = "DatoLogistico.findByFechaInicio", query = "SELECT d FROM DatoLogistico d WHERE d.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "DatoLogistico.findByFechaFin", query = "SELECT d FROM DatoLogistico d WHERE d.fechaFin = :fechaFin"),
    @NamedQuery(name = "DatoLogistico.findByIdUsuario", query = "SELECT d FROM DatoLogistico d WHERE d.idUsuario = :idUsuario"),
    @NamedQuery(name = "DatoLogistico.findById", query = "SELECT d FROM DatoLogistico d WHERE d.id = :id")})
public class DatoLogistico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "codigo_gtin")
    private String codigoGtin;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Basic(optional = false)
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatoLogistico")
    private Collection<ValorDatoLogistico> valorDatoLogisticoCollection;

    public DatoLogistico() {
    }

    public DatoLogistico(Integer id) {
        this.id = id;
    }

    public DatoLogistico(Integer id, String codigoGtin, Date fechaInicio, Date fechaFin, int idUsuario) {
        this.id = id;
        this.codigoGtin = codigoGtin;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.idUsuario = idUsuario;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<ValorDatoLogistico> getValorDatoLogisticoCollection() {
        return valorDatoLogisticoCollection;
    }

    public void setValorDatoLogisticoCollection(Collection<ValorDatoLogistico> valorDatoLogisticoCollection) {
        this.valorDatoLogisticoCollection = valorDatoLogisticoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatoLogistico)) {
            return false;
        }
        DatoLogistico other = (DatoLogistico) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.DatoLogistico[ id=" + id + " ]";
    }
    
}
