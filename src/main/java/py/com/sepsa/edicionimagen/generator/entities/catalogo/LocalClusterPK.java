/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class LocalClusterPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_cluster")
    private int idCluster;
    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @Column(name = "id_local")
    private int idLocal;

    public LocalClusterPK() {
    }

    public LocalClusterPK(int idCluster, int idPersona, int idLocal) {
        this.idCluster = idCluster;
        this.idPersona = idPersona;
        this.idLocal = idLocal;
    }

    public int getIdCluster() {
        return idCluster;
    }

    public void setIdCluster(int idCluster) {
        this.idCluster = idCluster;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCluster;
        hash += (int) idPersona;
        hash += (int) idLocal;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalClusterPK)) {
            return false;
        }
        LocalClusterPK other = (LocalClusterPK) object;
        if (this.idCluster != other.idCluster) {
            return false;
        }
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idLocal != other.idLocal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.LocalClusterPK[ idCluster=" + idCluster + ", idPersona=" + idPersona + ", idLocal=" + idLocal + " ]";
    }
    
}
