package py.com.sepsa.edicionimagen.generator.utils;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/** 
 * Clase utilizada para el manejo de logs
 * @author Daniel F. Escauriza Arza
 */
public class LogHandler {
    
    /**
     * Log de la aplicaci�n
     */
    private static Logger log;

    /**
     * Configura el archivo de log
     */    
    static {
        Properties properties= new Properties();

        properties.setProperty("log4j.appender.XmlTransfer", "org.apache.log4j.DailyRollingFileAppender");//
        properties.setProperty("log4j.appender.XmlTransfer.File", "/var/log/sincroniza-edicion-imagen.log");//
        properties.setProperty("log4j.appender.XmlTransfer.DatePattern", "'.'yyyy-MM-dd");//
        properties.setProperty("log4j.appender.XmlTransfer.layout", "org.apache.log4j.PatternLayout");//
        properties.setProperty("log4j.appender.XmlTransfer.layout.ConversionPattern", "%d{dd MMM yyyy HH:mm:ss,SSS} %p %t %c - %m%n");//
        properties.setProperty("log4j.logger.XmlTransfer", "DEBUG, XmlTransfer");//
        
        PropertyConfigurator.configure(properties);
        log = Logger.getLogger("XmlTransfer");
    }
    
    /**
     * Logea eventos tipo DEBUG
     * @param msg Mensaje de debug
     */
    public static void logDebug(String msg) {
        log.debug(String.format("%s", msg));
    }
    
    /**
     * Logea eventos tipo INFO
     * @param msg Mensaje de informaci�n
     */
    public static void logInfo(String msg) {
        log.info(String.format("%s", msg));
    }
    
    /**
     * Loguea eventos tipo WARN 
     * @param msg Mensaje de informaci�n
     */
    public static void logWarn(String msg) {
        log.warn(String.format("%s", msg));
    }
    
    /**
     * Logea eventos tipo ERROR
     * @param msg Mensaje de error
     */
    public static void logError(String msg) {
        log.error(String.format("%s", msg));
    }
    
    /**
     * Logea eventos tipo FATAL
     * @param msg Mensaje de error
     * @param ex Excepci�n producida
     */
    public static void logFatal(String msg, Throwable ex) {
        log.fatal(String.format("%s", msg), ex);
    }
    
    /**
     * Tipos de eventos log 
     */
    public enum LogType {
        DEBUG, INFO, WARNING, ERROR, FATAL;
    }
}
