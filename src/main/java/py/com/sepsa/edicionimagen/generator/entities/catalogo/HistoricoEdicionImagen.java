/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.Etiqueta;
import py.com.sepsa.edicionimagen.generator.entities.trans.Usuario;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "historico_edicion_imagen", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
public class HistoricoEdicionImagen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Usuario idUsuario;
    @JoinColumn(name = "id_usuario_editor", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Usuario idUsuarioEditor;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "nombre_archivo")
    private String nombreArchivo;
    @Basic(optional = false)
    @Column(name = "hash")
    private String hash;
    @Basic(optional = false)
    @Column(name = "dispositivo")
    private String dispositivo;
    @Basic(optional = false)
    @Column(name = "url")
    private String url;
    @JoinColumn(name = "id_edicion_imagen", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EdicionImagen idEdicionImagen;
    @JoinColumn(name = "id_etiqueta", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Etiqueta idEtiqueta;

    public HistoricoEdicionImagen() {
    }

    public HistoricoEdicionImagen(Integer id) {
        this.id = id;
    }

    public HistoricoEdicionImagen(Integer id, Date fecha, String nombreArchivo, String hash, String dispositivo, String url) {
        this.id = id;
        this.fecha = fecha;
        this.nombreArchivo = nombreArchivo;
        this.hash = hash;
        this.dispositivo = dispositivo;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Usuario getIdUsuarioEditor() {
        return idUsuarioEditor;
    }

    public void setIdUsuarioEditor(Usuario idUsuarioEditor) {
        this.idUsuarioEditor = idUsuarioEditor;
    }

    public EdicionImagen getIdEdicionImagen() {
        return idEdicionImagen;
    }

    public void setIdEdicionImagen(EdicionImagen idEdicionImagen) {
        this.idEdicionImagen = idEdicionImagen;
    }

    public void setIdEtiqueta(Etiqueta idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    public Etiqueta getIdEtiqueta() {
        return idEtiqueta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoEdicionImagen)) {
            return false;
        }
        HistoricoEdicionImagen other = (HistoricoEdicionImagen) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.HistoricoEdicionImagen[ id=" + id + " ]";
    }
    
}
