/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "calificador_descripcion_articulo", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CalificadorDescripcionArticulo.findAll", query = "SELECT c FROM CalificadorDescripcionArticulo c"),
    @NamedQuery(name = "CalificadorDescripcionArticulo.findByIdProducto", query = "SELECT c FROM CalificadorDescripcionArticulo c WHERE c.idProducto = :idProducto"),
    @NamedQuery(name = "CalificadorDescripcionArticulo.findById", query = "SELECT c FROM CalificadorDescripcionArticulo c WHERE c.id = :id")})
public class CalificadorDescripcionArticulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCalificadorDescripcionArticulo")
    private Collection<SegmentoPrecio> segmentoPrecioCollection;
    @JoinColumn(name = "id_codigo_pais", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CodigoPais idCodigoPais;
    @JoinColumn(name = "id_codigo_subdivision", referencedColumnName = "id")
    @ManyToOne
    private CodigoSubdivision idCodigoSubdivision;
    @JoinColumn(name = "id_sector", referencedColumnName = "id")
    @ManyToOne
    private Sector idSector;
    @JoinColumn(name = "id_relacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SegmentoRelacion idRelacion;

    public CalificadorDescripcionArticulo() {
    }

    public CalificadorDescripcionArticulo(Integer id) {
        this.id = id;
    }

    public CalificadorDescripcionArticulo(Integer id, int idProducto) {
        this.id = id;
        this.idProducto = idProducto;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<SegmentoPrecio> getSegmentoPrecioCollection() {
        return segmentoPrecioCollection;
    }

    public void setSegmentoPrecioCollection(Collection<SegmentoPrecio> segmentoPrecioCollection) {
        this.segmentoPrecioCollection = segmentoPrecioCollection;
    }

    public CodigoPais getIdCodigoPais() {
        return idCodigoPais;
    }

    public void setIdCodigoPais(CodigoPais idCodigoPais) {
        this.idCodigoPais = idCodigoPais;
    }

    public CodigoSubdivision getIdCodigoSubdivision() {
        return idCodigoSubdivision;
    }

    public void setIdCodigoSubdivision(CodigoSubdivision idCodigoSubdivision) {
        this.idCodigoSubdivision = idCodigoSubdivision;
    }

    public Sector getIdSector() {
        return idSector;
    }

    public void setIdSector(Sector idSector) {
        this.idSector = idSector;
    }

    public SegmentoRelacion getIdRelacion() {
        return idRelacion;
    }

    public void setIdRelacion(SegmentoRelacion idRelacion) {
        this.idRelacion = idRelacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalificadorDescripcionArticulo)) {
            return false;
        }
        CalificadorDescripcionArticulo other = (CalificadorDescripcionArticulo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CalificadorDescripcionArticulo[ id=" + id + " ]";
    }
    
}
