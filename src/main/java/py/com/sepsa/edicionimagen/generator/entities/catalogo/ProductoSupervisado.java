/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_supervisado", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoSupervisado.findAll", query = "SELECT p FROM ProductoSupervisado p"),
    @NamedQuery(name = "ProductoSupervisado.findById", query = "SELECT p FROM ProductoSupervisado p WHERE p.id = :id"),
    @NamedQuery(name = "ProductoSupervisado.findByIdUsuarioSupervisor", query = "SELECT p FROM ProductoSupervisado p WHERE p.idUsuarioSupervisor = :idUsuarioSupervisor"),
    @NamedQuery(name = "ProductoSupervisado.findByIdUsuarioAuditor", query = "SELECT p FROM ProductoSupervisado p WHERE p.idUsuarioAuditor = :idUsuarioAuditor"),
    @NamedQuery(name = "ProductoSupervisado.findByIdProducto", query = "SELECT p FROM ProductoSupervisado p WHERE p.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoSupervisado.findByFechaSupervision", query = "SELECT p FROM ProductoSupervisado p WHERE p.fechaSupervision = :fechaSupervision"),
    @NamedQuery(name = "ProductoSupervisado.findByComentario", query = "SELECT p FROM ProductoSupervisado p WHERE p.comentario = :comentario")})
public class ProductoSupervisado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_usuario_supervisor")
    private Integer idUsuarioSupervisor;
    @Column(name = "id_usuario_auditor")
    private Integer idUsuarioAuditor;
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "fecha_supervision")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSupervision;
    @Column(name = "comentario")
    private String comentario;
    @JoinColumn(name = "id_tipo_estado_carga", referencedColumnName = "id")
    @ManyToOne
    private TipoEstadoCarga idTipoEstadoCarga;

    public ProductoSupervisado() {
    }

    public ProductoSupervisado(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuarioSupervisor() {
        return idUsuarioSupervisor;
    }

    public void setIdUsuarioSupervisor(Integer idUsuarioSupervisor) {
        this.idUsuarioSupervisor = idUsuarioSupervisor;
    }

    public Integer getIdUsuarioAuditor() {
        return idUsuarioAuditor;
    }

    public void setIdUsuarioAuditor(Integer idUsuarioAuditor) {
        this.idUsuarioAuditor = idUsuarioAuditor;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Date getFechaSupervision() {
        return fechaSupervision;
    }

    public void setFechaSupervision(Date fechaSupervision) {
        this.fechaSupervision = fechaSupervision;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public TipoEstadoCarga getIdTipoEstadoCarga() {
        return idTipoEstadoCarga;
    }

    public void setIdTipoEstadoCarga(TipoEstadoCarga idTipoEstadoCarga) {
        this.idTipoEstadoCarga = idTipoEstadoCarga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoSupervisado)) {
            return false;
        }
        ProductoSupervisado other = (ProductoSupervisado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProductoSupervisado[ id=" + id + " ]";
    }
    
}
